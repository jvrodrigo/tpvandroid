package com.globales.tpvandroid.models;

public class Comanda {
	private String nombre = "";
	private int cantidad = 0;
	private String variedad = "";
	private String punto = "";
	private boolean impresa = false;
	private boolean comentario = false;
	private String tipo = "0";
	private double preciounidad;

	
	public Comanda(String name) {
		this.setNombre(name);
		this.setCantidad(1);
		this.setImpresa(false);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	} 

	public String getVariedad() {
		return variedad;
	}

	public void setVariedad(String variedad) {
		this.variedad = variedad;
	}

	public String getPunto() {
		return punto;
	}

	public void setPunto(String punto) {
		this.punto = punto;
	}

	public void anadirUno() {
		this.cantidad++;
	}

	public void quitarUno() {
		this.cantidad--;
	}

	public boolean isImpresa() {
		return impresa;
	}

	public void setImpresa(boolean impresa) {
		this.impresa = impresa;
	}

	public boolean isComentario() {
		return comentario;
	}

	public void setComentario(boolean comentario) {
		this.comentario = comentario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public double getPreciounidad() {
		return preciounidad;
	}

	public void setPreciounidad(double preciounidad) {
		this.preciounidad = preciounidad;
	}
	
	public void setPreciounidad(String preciounidad) {
		this.preciounidad = Double.parseDouble(preciounidad);
	}
}