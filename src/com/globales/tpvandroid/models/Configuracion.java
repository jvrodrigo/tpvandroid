package com.globales.tpvandroid.models;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.activities.IntentExploradorArchivos;
import com.globales.tpvandroid.dao.Conexion;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class Configuracion extends Activity {
	Conexion conex;
	TextView num_serie;
	EditText ipBBDD; //10.0.2.2 | 192.168.1.10
	EditText puerto; //3306
	EditText NombreBBDD; //Restaurante
	EditText UsuarioBBDD; //Globales
	EditText PassBBDD;	//Admin32
	EditText NumCaja;	//Admin32
	ImageButton Boton_Imagen;
	String selectedItem = "";
	
	protected static final int CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA = 1;

	protected EditText txtRutaArchivo;
    private FileOutputStream ficheroInstalacion;
	ImageButton botonIconoCarpeta;
	//EditText Imp_Bar; //192.168.0.201
	

	Handler handlerCam = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listacaj = (ArrayList<String>) bundle.get("Cajas");
			SeleccCajas(listacaj);
		}
	};


	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};

	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);

		conex = new Conexion(this);
		// IP en formato correcto
		ipBBDD = (EditText)findViewById(R.id.ip_bbdd);
		puerto = (EditText)findViewById(R.id.puerto_bbdd);
		NombreBBDD = (EditText)findViewById(R.id.nombre_bbdd);
		UsuarioBBDD = (EditText)findViewById(R.id.usuario_bbdd);
		PassBBDD = (EditText)findViewById(R.id.pwd_bbdd);
		NumCaja = (EditText)findViewById(R.id.numCaja);
		Boton_Imagen = (ImageButton) findViewById(R.id.boton_cambio_logo);
		num_serie = (TextView) findViewById(R.id.tv_numero_disp);
		txtRutaArchivo = (EditText) findViewById(R.id.ruta_archivo); 
        ImageButton botonIconoCarpeta = (ImageButton) findViewById(R.id.boton_cambio_logo);
		 
        botonIconoCarpeta.setOnClickListener(new View.OnClickListener() {
		      public void onClick(View arg0) {
	                    abrirArchivo(); 
		      }
	        });
			
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					android.text.Spanned dest, int dstart, int dend) {
				if (end > start) {
					String destTxt = dest.toString();
					String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
					if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
						return "";
					} else {
						String[] splits = resultingTxt.split("\\.");
						for (int i=0; i<splits.length; i++) {
							if (Integer.valueOf(splits[i]) > 255) {
								return "";
							}
						}
					}
				}
				return null;
			}
		};
		ipBBDD.setFilters(filters);

		SharedPreferences prefe=getSharedPreferences("datos",Context.MODE_PRIVATE);
		ipBBDD.setText(prefe.getString("ip",""));
		puerto.setText(prefe.getString("puertoBBDD",""));
		NombreBBDD.setText( prefe.getString("nombreBBDD",""));
		UsuarioBBDD.setText(prefe.getString("UsuarioBBDD",""));
		PassBBDD.setText(prefe.getString("PwdBBDD",""));
		NumCaja.setText(prefe.getString("NumCaja",""));
		txtRutaArchivo.setText(prefe.getString("RutaLogo",""));
		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		num_serie.setText(tManager.getDeviceId()) ;
}


	public void ListarCajas(View view) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {      
					Connection conn = conex.Conectar();
					String stsql = "Select NUM_CAJA FROM tpvs";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					ArrayList<String> listaCajas = new ArrayList<String>();
					while (rs.next()){
						listaCajas.add(rs.getString(1));
					}
					Message msg = handlerCam.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("Cajas", listaCajas);
					msg.setData(bundle);  		             		
					handlerCam.sendMessage(msg);
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}
	

	private void SeleccCajas(ArrayList<String> listacaj) {
		if (listacaj.size()!=0){
			final String[] items = listacaj.toArray(new String[listacaj.size()]);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Seleccione la Caja");
			selectedItem = items[0];
			builder.setCancelable(true);

			builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					selectedItem = items[which];
				}
			});

			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					NumCaja.setText(selectedItem);
				}
			});
			builder.show();
		}
	}
	
	
	public void confirma(View view) {
		SharedPreferences preferencias=getSharedPreferences("datos",Context.MODE_PRIVATE);
		Editor editor=preferencias.edit();
		editor.putString("ip", ipBBDD.getText().toString());
		editor.putString("puertoBBDD", puerto.getText().toString());
		editor.putString("nombreBBDD", NombreBBDD.getText().toString());
		editor.putString("UsuarioBBDD", UsuarioBBDD.getText().toString());
		editor.putString("PwdBBDD", PassBBDD.getText().toString());
		editor.putString("NumCaja", NumCaja.getText().toString());
		editor.putString("RutaLogo", txtRutaArchivo.getText().toString());
		editor.commit();
		finish();		
	}

	
	
	//Buscar imagen a poner como logo en la pagina principal.
	    
	  private boolean guardarRawComo(String nombreFichero){ 

		     // Cargar en memoria el fichero de instalaci�n del explorador, filemanager.apk,
		     // que est� incorporado al proyecto en la subcarpeta de recursos /res/raw 
		     byte[] buffer=null;  
		     InputStream ficheroEntrada = getResources().openRawResource(R.raw.filemanager);
		     int size=0;  
		     // Para ello se construye un array de octetos en el que se almacena el fichero
		     // byte a byte.
		     try {  
		      size = ficheroEntrada.available();  
		      buffer = new byte[size];  
		      ficheroEntrada.read(buffer);  
		      ficheroEntrada.close();  
		     } catch (IOException e) {  
		         return false;  
		     }  

		     // Construir la ruta de la SD card en la que se almacenar� una copia del fichero
		     // de instalaci�n. Por simplicidad lo guardaremos directamente como /sdcard/<nombreFichero>.apk
		     String ficheroAPK=nombreFichero+".apk";
		     String directorioBase = Environment.getExternalStorageDirectory().getAbsolutePath();
		     String rutaCompleta = directorioBase + File.separator + ficheroAPK;

		     // Volcar el array de bytes a la nueva copia del fichero en la SD card.
		     boolean existeFicheroInstalacion = (new File(rutaCompleta)).exists();  
		     if (!existeFicheroInstalacion){ 
		           
		         try {  
		          ficheroInstalacion = new FileOutputStream(rutaCompleta);  
		          ficheroInstalacion.write(buffer);  
		          ficheroInstalacion.flush();  
		          ficheroInstalacion.close();  
		         } catch (FileNotFoundException e) {  
		             return false;
		         } catch (IOException e) {  
		             return false;  
		         }  
		         return true;
		     }
		     return false;
		  }  
		    
		    
		    /**
		     * Llamar al gestor de archivos para seleccionar un archivo que ser� abierto.
		     */
		    private void abrirArchivo() {
		        
		            String nombreArchivo = txtRutaArchivo.getText().toString();

		            Intent intent = new Intent(IntentExploradorArchivos.ACCION_SELECCIONAR_ARCHIVO);

		            // Construir la URI a partir del nombre completo del fichero.
		            intent.setData(Uri.parse("file://" + nombreArchivo));

		            // Personalizar el t�tulo del explorador de archivos y el texto del bot�n
		            intent.putExtra(IntentExploradorArchivos.EXTRA_TITULO, getString(R.string.titulo_logo));
		            intent.putExtra(IntentExploradorArchivos.EXTRA_TEXTO_BOTON, getString(R.string.texto_boton_abrir));

		            try {
		                    startActivityForResult(intent, CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA);
		            } catch (ActivityNotFoundException e) {
		                   guardarRawComo("filemanager"); 
		                   Intent intentInstalacion = new Intent(Intent.ACTION_VIEW);
		                   String ruta=Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator +"filemanager.apk";
		                   intentInstalacion.setDataAndType(Uri.fromFile(new File(ruta)), "application/vnd.android.package-archive");
		                   startActivityForResult(intentInstalacion,0);   
		       
		            }
			}

		    
			@Override
			protected void onActivityResult(int codigoSolicitud, int codigoResultado, Intent datos) {
				super.onActivityResult(codigoSolicitud, codigoResultado, datos);

				switch (codigoSolicitud) {
				case CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA:
					if (codigoResultado == RESULT_OK){

		                           
		                           if (datos != null) {
						// obtener el nombre del archivo
						String nombreArchivo = datos.getDataString();
						if (nombreArchivo != null) {
							// Eliminar el prefijo "file://" del URI
							if (nombreArchivo.startsWith("file://")) {
								nombreArchivo = nombreArchivo.substring(7);
							}
							
							txtRutaArchivo.setText(nombreArchivo);
						}
					   }
		                           }
					break;
		                        default: {
		                             File fichEliminarInstalacion=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator +"filemanager.apk");
		                             if (fichEliminarInstalacion!=null)
		                                    fichEliminarInstalacion.delete(); 
		                             }
		                           break;
		                        
				}
			}
	


	 

	public void cerrar(View view) {
		finish();
	}

	
	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}
/*
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.example.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

@SuppressLint("HandlerLeak")
public class Configuracion extends Activity {
	Conexion conex;
	TextView num_serie;
	EditText ipBBDD; //10.0.2.2
	EditText puerto; //3306
	EditText NombreBBDD; //Restaurante
	EditText UsuarioBBDD; //Globales
	EditText PassBBDD;	//Admin32
	EditText NumCaja;	//Admin32
	String selectedItem = "";
	ImageButton Boton_Imagen;
	//EditText Imp_Bar; //192.168.0.201

	Handler handlerCam = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listacaj = (ArrayList<String>) bundle.get("Cajas");
			SeleccCajas(listacaj);
		}
	};


	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};

	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);

		conex = new Conexion(this);
		// IP en formato correcto
		ipBBDD = (EditText)findViewById(R.id.ip_bbdd);
		puerto = (EditText)findViewById(R.id.puerto_bbdd);
		NombreBBDD = (EditText)findViewById(R.id.nombre_bbdd);
		UsuarioBBDD = (EditText)findViewById(R.id.usuario_bbdd);
		PassBBDD = (EditText)findViewById(R.id.pwd_bbdd);
		Boton_Imagen = (ImageButton) findViewById(R.id.boton_cambio_logo);
		NumCaja = (EditText)findViewById(R.id.numCaja);
		
		num_serie = (TextView) findViewById(R.id.tv_numero_disp);
		
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					android.text.Spanned dest, int dstart, int dend) {
				if (end > start) {
					String destTxt = dest.toString();
					String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
					if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) { 
						return "";
					} else {
						String[] splits = resultingTxt.split("\\.");
						for (int i=0; i<splits.length; i++) {
							if (Integer.valueOf(splits[i]) > 255) {
								return "";
							}
						}
					}
				}
				return null;
			}
		};
		ipBBDD.setFilters(filters);
		
		Boton_Imagen.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				BusquedaImagen();
			}
		});

		SharedPreferences prefe=getSharedPreferences("datos",Context.MODE_PRIVATE);
		ipBBDD.setText(prefe.getString("ip",""));
		puerto.setText(prefe.getString("puertoBBDD",""));
		NombreBBDD.setText( prefe.getString("nombreBBDD",""));
		UsuarioBBDD.setText(prefe.getString("UsuarioBBDD",""));
		PassBBDD.setText(prefe.getString("PwdBBDD",""));
		NumCaja.setText(prefe.getString("NumCaja",""));

		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		num_serie.setText(tManager.getDeviceId()) ;
}


	public void ListarCajas(View view) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {      
					Connection conn = conex.Conectar();
					String stsql = "Select NUM_CAJA FROM tpvs";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					ArrayList<String> listaCajas = new ArrayList<String>();
					while (rs.next()){
						listaCajas.add(rs.getString(1));
					}
					Message msg = handlerCam.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("Cajas", listaCajas);
					msg.setData(bundle);  		             		
					handlerCam.sendMessage(msg);
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}
	

	private void SeleccCajas(ArrayList<String> listacaj) {
		if (listacaj.size()!=0){
			final String[] items = listacaj.toArray(new String[listacaj.size()]);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Seleccione la Caja");
			selectedItem = items[0];
			builder.setCancelable(true);

			builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					selectedItem = items[which];
				}
			});

			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					NumCaja.setText(selectedItem);
				}
			});
			builder.show();
		}
	}
	
	
	public void confirma(View view) {
		SharedPreferences preferencias=getSharedPreferences("datos",Context.MODE_PRIVATE);
		Editor editor=preferencias.edit();
		editor.putString("ip", ipBBDD.getText().toString());
		editor.putString("puertoBBDD", puerto.getText().toString());
		editor.putString("nombreBBDD", NombreBBDD.getText().toString());
		editor.putString("UsuarioBBDD", UsuarioBBDD.getText().toString());
		editor.putString("PwdBBDD", PassBBDD.getText().toString());
		editor.putString("NumCaja", NumCaja.getText().toString());
		editor.commit();
		finish();		
	}
	//Buscar imagen a poner como logo en la pagina principal.
	public void  BusquedaImagen(){
		Intent GestorArchivos = new Intent();
		GestorArchivos.setClass(Configuracion.this,GestorArchivos.class); 
		startActivity(GestorArchivos);
	}


	public void cerrar(View view) {
		finish();
	}

	
	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}*/