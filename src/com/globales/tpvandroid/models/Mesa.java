package com.globales.tpvandroid.models;

import android.graphics.Point;

public class Mesa {
	private String id = "";
	private int left = 0;
	private int top = 0;
	private int ancho = 0;
	private int alto = 0;
	private String tarifa = "";
	private Point posicion;
	
	public Mesa (){
		this.left = 0;
		this.top = 0;
		this.ancho = 0;
		this.alto = 0;	
	}
	public Mesa getMesa(){
		
		return this;
	}
	public Point getPosicion(){
		return posicion;
	}
	public void setPosicion(Point posicion){
		this.posicion = posicion;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getLeft() {
		return left;
	}
	public void setLeft(int left) {
		this.left = left;
	}
	
	public int getTop() {
		return top;
	}
	public void setTop(int top) {
		this.top = top;
	}
	public int getAncho() {
		return ancho;
	}
	public void setAncho(int ancho) {
		this.ancho = ancho;
	}
	public int getAlto() {
		return alto;
	}
	public void setAlto(int alto) {
		this.alto = alto;
	}
	public String getTarifa() {
		return tarifa;
	}
	public void setTarifa(String tarifa) {
		this.tarifa = tarifa;
	}
}
