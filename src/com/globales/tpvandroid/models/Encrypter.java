package com.globales.tpvandroid.models;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.annotation.SuppressLint;
import android.util.Base64;

@Deprecated
public class Encrypter {
	private String key;
	private String initializationVector;
	
	public Encrypter(String key, String initializationVector)
	{
		this.key = key;
		this.initializationVector = initializationVector;
	}

	@SuppressLint("TrulyRandom")
	public String encryptText(String plainText) throws Exception{
		//----  Use specified 3DES key and IV from other source --------------
		byte[] plaintext = plainText.getBytes();
		byte[] tdesKeyData = key.getBytes();

		byte[] myIV = initializationVector.getBytes();

		Cipher c3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
		SecretKeySpec myKey = new SecretKeySpec(tdesKeyData, "DESede");
		IvParameterSpec ivspec = new IvParameterSpec(myIV);

		c3des.init(Cipher.ENCRYPT_MODE, myKey, ivspec);
		byte[] cipherText = c3des.doFinal(plaintext);

		return Base64.encodeToString(cipherText,Base64.DEFAULT);
	}
}