/*package com.globales.tpvandroid.models;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import com.example.tpvandroid.R;
import com.globales.tpvandroid.activities.IntentExploradorArchivos;

public class GestorArchivos extends Activity {

	protected static final int CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA = 1;

	protected EditText txtRutaArchivo;
	private FileOutputStream ficheroInstalacion;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configuracion);

		txtRutaArchivo = (EditText) findViewById(R.id.ruta_archivo);

		ImageButton botonIconoCarpeta = (ImageButton) findViewById(R.id.boton_cambio_logo);
		botonIconoCarpeta.setOnClickListener(new View.OnClickListener() {

			public void onClick(View arg0) {
				abrirArchivo();
			}
		});

	}

	private boolean guardarRawComo(String nombreFichero) {

		// Cargar en memoria el fichero de instalaci�n del explorador,
		// filemanager.apk,
		// que est� incorporado al proyecto en la subcarpeta de recursos
		// /res/raw
		byte[] buffer = null;
		InputStream ficheroEntrada = getResources().openRawResource(R.raw.filemanager);
		int size = 0;
		// Para ello se construye un array de octetos en el que se almacena el
		// fichero
		// byte a byte.
		try {
			size = ficheroEntrada.available();
			buffer = new byte[size];
			ficheroEntrada.read(buffer);
			ficheroEntrada.close();
		} catch (IOException e) {
			return false;
		}

		// Construir la ruta de la SD card en la que se almacenar� una copia del
		// fichero
		// de instalaci�n. Por simplicidad lo guardaremos directamente como
		// /sdcard/<nombreFichero>.apk
		String ficheroAPK = nombreFichero + ".apk";
		String directorioBase = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		String rutaCompleta = directorioBase + File.separator + ficheroAPK;

		// Volcar el array de bytes a la nueva copia del fichero en la SD card.
		boolean existeFicheroInstalacion = (new File(rutaCompleta)).exists();
		if (!existeFicheroInstalacion) {

			try {
				ficheroInstalacion = new FileOutputStream(rutaCompleta);
				ficheroInstalacion.write(buffer);
				ficheroInstalacion.flush();
				ficheroInstalacion.close();
			} catch (FileNotFoundException e) {
				return false;
			} catch (IOException e) {
				return false;
			}
			return true;
		}
		return false;
	}

	/**
	 * Llamar al gestor de archivos para seleccionar un archivo que ser�
	 * abierto.
	 *//*
	private void abrirArchivo() {

		String nombreArchivo = txtRutaArchivo.getText().toString();

		Intent intent = new Intent(
				IntentExploradorArchivos.ACCION_SELECCIONAR_ARCHIVO);

		// Construir la URI a partir del nombre completo del fichero.
		intent.setData(Uri.parse("file://" + nombreArchivo));

		// Personalizar el t�tulo del explorador de archivos y el texto del
		// bot�n
		intent.putExtra(IntentExploradorArchivos.EXTRA_TITULO,
				getString(R.string.titulo_logo));
		intent.putExtra(IntentExploradorArchivos.EXTRA_TEXTO_BOTON,
				getString(R.string.texto_boton_abrir));

		try {
			startActivityForResult(intent,
					CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA);
		} catch (ActivityNotFoundException e) {
			guardarRawComo("filemanager");
			Intent intentInstalacion = new Intent(Intent.ACTION_VIEW);
			String ruta = Environment.getExternalStorageDirectory()
					.getAbsolutePath() + File.separator + "filemanager.apk";
			intentInstalacion.setDataAndType(Uri.fromFile(new File(ruta)),
					"application/vnd.android.package-archive");
			startActivityForResult(intentInstalacion, 0);
		}
	}

	@Override
	protected void onActivityResult(int codigoSolicitud, int codigoResultado,
			Intent datos) {
		super.onActivityResult(codigoSolicitud, codigoResultado, datos);

		switch (codigoSolicitud) {
		case CODIGO_SOLICITUD_SELECCIONAR_ARCHIVO_O_CARPETA:
			if (codigoResultado == RESULT_OK) {

				if (datos != null) {
					// obtener el nombre del archivo
					String nombreArchivo = datos.getDataString();
					if (nombreArchivo != null) {
						// Eliminar el prefijo "file://" del URI
						if (nombreArchivo.startsWith("file://")) {
							nombreArchivo = nombreArchivo.substring(7);
						}

						txtRutaArchivo.setText(nombreArchivo);
					}
				}
			}
			break;
		default: {
			File fichEliminarInstalacion = new File(Environment
					.getExternalStorageDirectory().getAbsolutePath()
					+ File.separator + "filemanager.apk");
			if (fichEliminarInstalacion != null)
				fichEliminarInstalacion.delete();
		}
			break;

		}
	}
}*/