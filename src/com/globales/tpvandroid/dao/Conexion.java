package com.globales.tpvandroid.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import android.content.Context;
import android.content.SharedPreferences;

public class Conexion {
	String host; // = "10.0.2.2"; //"192.168.1.10";
	String puerto;// = "3306";
	String BBDD;// = "Restaurante";
	String Usuario;// = "Globales";
	String Pass;// = "Admin32"; 
	//192.168.0.201 << impresora pruebas
	
	public Conexion(Context context) {
		SharedPreferences prefe = context.getSharedPreferences("datos",Context.MODE_PRIVATE);
		host = prefe.getString("ip","");
		puerto = prefe.getString("puertoBBDD","");
		BBDD = prefe.getString("nombreBBDD","");
		Usuario = prefe.getString("UsuarioBBDD","");
		Pass = prefe.getString("PwdBBDD","");
	}

	public Connection Conectar() throws Exception {
		try {		
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://" + host + ":"+ puerto +"/"+ BBDD , Usuario, Pass);
			return conn;
		} catch (SQLException se) {
			throw new Exception(se);
		} catch (ClassNotFoundException e) {
			throw new Exception(e);    		   
		}
	}
}