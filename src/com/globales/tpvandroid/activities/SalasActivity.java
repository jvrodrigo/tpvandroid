package com.globales.tpvandroid.activities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;


@SuppressLint("HandlerLeak")
public class SalasActivity extends Activity {
	String usuario;
	Conexion conex;
	String tipoPedido;

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			String mesas = bundle.getString("mesas");
			String sala = bundle.getString("nombre_sala");
			MostrarMesas(sala,mesas);
		}
	};


	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_salas);
		TableLayout layout = (TableLayout)findViewById(R.id.SalasLayout);

		conex = new Conexion(this);
		Bundle bundle = getIntent().getExtras();
		usuario = bundle.getString("usuario");
		tipoPedido = bundle.getString("tipoPedido");
		String[] salasRecibidas = bundle.getString("salas").split("\n");	
		List<String> ListaSalas = Arrays.asList(salasRecibidas);

		int indice = 0;
		int BotPorFila = 2;

		while (indice < ListaSalas.size()){
			TableRow tr = new TableRow(this);
			tr.setLayoutParams(new TableRow.LayoutParams
					(TableRow.LayoutParams.MATCH_PARENT,
							TableRow.LayoutParams.WRAP_CONTENT));
			for (int i=0; i<BotPorFila;i++){
				if (indice < ListaSalas.size()){
					final Button btn = new Button(this);
					btn.setText(ListaSalas.get(indice));
					btn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							pasarSalas(btn.getText().toString());
						}
					});
					tr.addView(btn);  
					indice++;         			
				}
			}
			layout.addView(tr);         
		}
	}


	public void pasarSalas(final String nombreSala) {
		Runnable runnable = new Runnable() { 
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT DEFINICION FROM salas WHERE SALA = '" + nombreSala + "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					rs.next();
					String resultado = rs.getString(1);
					conn.close();

					Message msg = handler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("mesas", resultado);
					bundle.putString("nombre_sala", nombreSala);
					msg.setData(bundle);  		             		
					handler.sendMessage(msg);

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}; 
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	} 


	public void MostrarMesas(String sala, String mesas) {
		Intent i = new Intent(this, MesasActivity.class);
		i.putExtra("mesas", mesas);
		i.putExtra("usuario", usuario);
		i.putExtra("sala", sala);
		i.putExtra("tipoPedido",tipoPedido);
		startActivity(i);
	}


	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}