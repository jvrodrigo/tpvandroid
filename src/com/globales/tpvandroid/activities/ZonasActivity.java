package com.globales.tpvandroid.activities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch; 

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

@SuppressLint("HandlerLeak")
public class ZonasActivity extends Activity {
	Conexion conex;
	String usuario;

	
	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};


	Handler zonahandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			@SuppressWarnings("unchecked")
			ArrayList<String> ListaZonas = 
			(ArrayList<String>) bundle.get("zonas");
			DibujarZonas(ListaZonas);
		}
	};


	Handler salashandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			String salas = bundle.getString("salas");
			String tipoPedido = bundle.getString("tipoPedido");
			MostrarSalas(salas,tipoPedido);
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zonas);

		conex = new Conexion(this);
		Bundle bundle = getIntent().getExtras();
		usuario = bundle.getString("usuario");

		CrearZonas();
	}


	private void CrearZonas() {
		Runnable runnable = new Runnable() { 
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT ZONA FROM zonas z " +
							"WHERE z.ZONA IN (SELECT ZONA FROM salas) " +
							"AND EXISTS " +
							"(SELECT SALA FROM salas s " +
							"WHERE DEFINICION IS NOT NULL AND ZONA = z.ZONA " +
							"AND s.BLOQUEADA = 0)";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);  	 

					ArrayList<String> Zonas = new ArrayList<String>();
					while(rs.next()){
						Zonas.add(rs.getString(1));
					}

					conn.close();
					Message msg = zonahandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("zonas", Zonas);
					msg.setData(bundle);  		             		
					zonahandler.sendMessage(msg);

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}; 
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	} 


	private void DibujarZonas(ArrayList<String> ListaZonas) {
		int indice = 0;
		int BotPorFila = 2;
		TableLayout tablalay = (TableLayout)findViewById(R.id.ZonasLayout);

		while (indice < ListaZonas.size()){
			TableRow tr = new TableRow(this);
			tr.setLayoutParams(new TableRow.LayoutParams
					(TableRow.LayoutParams.MATCH_PARENT,
							TableRow.LayoutParams.WRAP_CONTENT));

			for (int i=0; i<BotPorFila;i++){
				if (indice < ListaZonas.size()){
					final Button btn = new Button(this);

					btn.setText(ListaZonas.get(indice));
					btn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							pasarSalas(btn.getText().toString());
						}
					});
					tr.addView(btn);  
					indice++;         			
				}				
			} 
			tablalay.addView(tr);         
		}
	}


	public void pasarSalas(final String zona) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT SALA FROM salas s " +
							"WHERE DEFINICION IS NOT NULL AND ZONA ='" + zona +
							"' AND s.BLOQUEADA = 0";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					String resultado = "";
					while(rs.next()) {
						resultado += rs.getString(1) + "\n";
					}
					
					String tipoPedido = "";
					stsql = "SELECT TIPO_PEDIDO FROM zonas WHERE ZONA = '"+ zona +"'";
					ResultSet rs2 = st.executeQuery(stsql);
					if (rs2.next()) {
						tipoPedido = rs2.getString(1);
					}				
					
					conn.close();
					Message msg = salashandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("salas", resultado);
					bundle.putString("tipoPedido",tipoPedido);
					msg.setData(bundle);
					salashandler.sendMessage(msg);

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}; 
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}


	public void MostrarSalas(String salas, String tipoPedido) {
		Intent i = new Intent(this, SalasActivity.class);
		i.putExtra("salas", salas);
		i.putExtra("usuario", usuario);
		i.putExtra("tipoPedido",tipoPedido);
		startActivity(i);
	}


	@Override
	public void finish() {
		try {
			final CountDownLatch latchfin = new CountDownLatch(1);
			Desbloquear (latchfin);
			latchfin.await();
		} catch (InterruptedException e) {
			MensajeError(e.getMessage());
		} finally {		
			super.finish();
		}
	}


	private void Desbloquear(final CountDownLatch latchfin) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					SharedPreferences prefe=getSharedPreferences("datos",Context.MODE_PRIVATE);
					String num_caja = prefe.getString("NumCaja","");

					String stsql = "UPDATE tpvs SET BLOQUEO = 0 " +
							"WHERE NUM_CAJA = '" + num_caja + "'";
					Statement st = conn.createStatement();
					st.executeUpdate(stsql); 
					latchfin.countDown();
					conn.close();

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	
		
	}


	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

}