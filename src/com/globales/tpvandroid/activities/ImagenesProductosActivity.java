package com.globales.tpvandroid.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.CountDownLatch;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class ImagenesProductosActivity extends Activity {
	Conexion conex;
	String Producto;
	Bundle bundle;
	ImageView imageView;
	Connection conn;
	CountDownLatch latchImagen;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_imagenes_productos);
		imageView = (ImageView) findViewById(R.id.layout_imagen_view);
		conex = new Conexion(this);
		bundle = getIntent().getExtras();

		Producto = transformaCadena(bundle.getString("Producto"));
		bundle.putString("producto", Producto);
		Button btnIrAFoto = (Button) findViewById(R.id.btnIrAFoto);
		btnIrAFoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i("ImagenesProductosActivity", "Conectando la camara");

				Intent camaraActivity = new Intent();
				camaraActivity.setClass(ImagenesProductosActivity.this,
						CamaraActivity.class);
				camaraActivity.putExtras(bundle);
				startActivity(camaraActivity);

			}
		});
		Button btnAtras = (Button) findViewById(R.id.btnAtras);
		btnAtras.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		try {
			
			Log.i("ImagenesProductosActivity", "conexion");
			/*
			 * String uri = "@drawable/" + Producto; int imageResource =
			 * getResources().getIdentifier(uri, "drawable", getPackageName());
			 * 
			 * Drawable res = getResources().getDrawable(imageResource);
			 * imageView.setImageDrawable(res);
			 * 
			 * // imageView.setImageResource(imageResource);
			 */
			latchImagen = new CountDownLatch(1);
			obtenerImagen();
			latchImagen.await();
			
			

		} catch (Exception e) {
			Toast.makeText(
					getApplicationContext(),
					"No coincide ninguna imagen con dicho producto mostrando LOGO",
					Toast.LENGTH_LONG).show();
			String uri = "@drawable/logo_empresa";
			int imageResource = getResources().getIdentifier(uri, "drawable",
					getPackageName());
			Drawable res = getResources().getDrawable(imageResource);
			imageView.setImageDrawable(res);
			Log.i("ImagenesProductosActivity", e.toString());
		}
	}

	public void obtenerImagen() {
		Runnable runnable = new Runnable() {
			public void run() {
				
				try {
					String stsql = "SELECT ICONO FROM productos WHERE producto = '"	+ bundle.getString("Producto") + "' and  ICONO != '';";
					Log.i("Consulta", stsql);
					conn = conex.Conectar();
					
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);

					int count = 0;
					
					while (rs.next()) {
						File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"TPVAndroid");
						File mediaFile = new File(mediaStorageDir.getPath() + File.separator
								+ transformaCadena(bundle.getString("Producto")) + ".jpg");
						FileOutputStream fos = new FileOutputStream(mediaFile);
						byte[] buffer = new byte[1];
						InputStream is = rs.getBinaryStream(1);
						while (is.read(buffer) > 0) {
							fos.write(buffer);
						}
						fos.close();
						Bitmap bitmap = BitmapFactory.decodeFile(mediaFile.getAbsolutePath());
						imageView.setImageBitmap(bitmap);
						count++;
					}
					latchImagen.countDown();
					conn.close();
					Log.i("ImagenesProductosActivity","Conexion cerrada");
					Log.i("Count -> ", String.valueOf(count));
					if(count==0){
						Log.i("ImagenesProductosActivity", "Imagen LOGO Empresa");
						/*Toast.makeText(
								getApplicationContext(),
								"No coincide ninguna imagen con dicho producto mostrando LOGO",
								Toast.LENGTH_LONG).show();*/
						String uri = "@drawable/logo_empresa";
						int imageResource = getResources().getIdentifier(uri, "drawable",
								getPackageName());
						Drawable res = getResources().getDrawable(imageResource);
						imageView.setImageDrawable(res);
					}
				
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			}
		};
		try{
			Thread sqlThread = new Thread(runnable);
			sqlThread.start();
			
		}catch(Exception e){
			
			Log.i("ExcductosActivity", e.toString());


		}
	}
	
	@SuppressLint("DefaultLocale")
	private String transformaCadena(String producto) {
		producto = producto.toLowerCase();
		producto = producto.replace("/", "_");
		producto = producto.replace(" ", "_");
		producto = producto.replace("�", "n");
		producto = producto.replace("+", "_");

		return producto;
	}
	/*
	private class DescargarImagen extends AsyncTask<Connection, Integer, Long> {
	     protected Long doInBackground(URL... urls) {
	         int count = urls.length;
	         long totalSize = 0;
	         for (int i = 0; i < count; i++) {
	             totalSize += Downloader.downloadFile(urls[i]);
	             publishProgress((int) ((i / (float) count) * 100));
	             // Escape early if cancel() is called
	             if (isCancelled()) break;
	         }
	         return totalSize;
	     }

	     protected void onProgressUpdate(Integer... progress) {
	         setProgressPercent(progress[0]);
	     }

	     protected void onPostExecute(Long result) {
	         showDialog("Downloaded " + result + " bytes");
	     }
	 }
	 
	 *
	 *
	 *runOnUiThread(new Runnable() {
				public void run() {
					
					try {
						String stsql = "SELECT ICONO FROM productos WHERE producto = '"	+ bundle.getString("Producto") + "';";
						Log.i("Consulta", stsql);
						conn = conex.Conectar();
						
						Statement st = conn.createStatement();
						ResultSet rs = st.executeQuery(stsql);
						
						// File file = new File("vv");
						//Log.i("ImagenesProductosActivity", rs.toString());
						while (rs.next()) {
							File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"TPVAndroid");
							File mediaFile = new File(mediaStorageDir.getPath() + File.separator
									+ transformaCadena(bundle.getString("Producto")) + ".jpg");
							FileOutputStream fos = new FileOutputStream(mediaFile);
							byte[] buffer = new byte[1];
							InputStream is = rs.getBinaryStream(1);
							while (is.read(buffer) > 0) {
								fos.write(buffer);
							}
							fos.close();
							Bitmap bitmap = BitmapFactory.decodeFile(mediaFile.getAbsolutePath());
							imageView.setImageBitmap(bitmap);
						}
						latchImagen.countDown();
						conn.close();
						Log.i("ImagenesProductosActivity","Conexion cerrada");
						
					} catch (Exception e) {
						Log.i("ImagenesProductosActivity", e.toString());
						e.printStackTrace();
					}
				
				}
			});
			latchImagen.await();
			//Thread sqlThread = new Thread(runnable);
			//sqlThread.start();
	 */

}
