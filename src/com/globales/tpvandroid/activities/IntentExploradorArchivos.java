package com.globales.tpvandroid.activities;

public final class IntentExploradorArchivos {

	public static final String ACCION_SELECCIONAR_ARCHIVO = "org.openintents.action.PICK_FILE";

	public static final String ACCION_SELECCIONAR_DIRECTORIO = "org.openintents.action.PICK_DIRECTORY";

	public static final String EXTRA_TITULO = "org.openintents.extra.TITLE";

	public static final String EXTRA_TEXTO_BOTON = "org.openintents.extra.BUTTON_TEXT";

}