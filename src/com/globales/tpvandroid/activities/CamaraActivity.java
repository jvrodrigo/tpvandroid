package com.globales.tpvandroid.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.CountDownLatch;



import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;
import com.mysql.jdbc.PreparedStatement;

public class CamaraActivity extends Activity {
	Conexion conex;
	String Producto;
	public static Bundle bundle;
	private Camera mCamera;
	private CamaraPreview mPreview;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_VIDEO = 2;
	private static final String TAG = MainActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camara);
		//Log.i("Camara", "Intentando acceder Cammmmmmmmmmmmmmm");
		// Create an instance of Camera
		mCamera = getCameraInstance();
		bundle = getIntent().getExtras();
		Producto = bundle.getString("Producto");
		try {
			conex = new Conexion(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Create our Preview view and set it as the content of our activity.
		mPreview = new CamaraPreview(this, mCamera);
		FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
		preview.addView(mPreview);
		// Add a listener to the Capture button
		Button captureButton = (Button) findViewById(R.id.button_capture);
		captureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Pulsado");
				// get an image from the camera
				Parameters params = mCamera.getParameters();
				params.setPictureSize(100, 100);
				mCamera.setParameters(params);
				mCamera.takePicture(null, null, mPicture);
			}
		});
	}

	private final PictureCallback mPicture = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
			if (pictureFile == null) {
				Log.d(TAG,
						"Error creating media file, check storage permissions");
				return;
			}

			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				fos.write(data);
				fos.close();
				final CountDownLatch latchGuardarImagen = new CountDownLatch(
						1);
				guardarImagen(latchGuardarImagen,pictureFile);
				Log.d(TAG, "onPictureTaken");
			} catch (FileNotFoundException e) {
				Log.d(TAG, "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d(TAG, "Error accessing file: " + e.getMessage());
			}
			Log.d(TAG, "SalgoOnPictureTaken");

		}
	};
	public void guardarImagen(final CountDownLatch latchimp,final File pictureFile) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					FileInputStream streamEntrada = new FileInputStream(pictureFile);
					Connection conn;
					conn = conex.Conectar();
					PreparedStatement pstmt = (PreparedStatement) conn
							.prepareStatement("UPDATE productos "
									+ "SET ICONO = (?) WHERE PRODUCTO = '"
									+ Producto + "';");
					//Log.i("Fotoooo","UPDATE productos "+ "SET ICONO = (?) WHERE PRODUCTO = '"	+ Producto + "';");
					pstmt.setBinaryStream(1, streamEntrada,
							(int) pictureFile.length());
					pstmt.executeUpdate();
					pstmt.close();
					streamEntrada.close();
				}catch(Exception e){
					Log.i("Foto","UPDATE productos " + "SET ICONO = (?) WHERE PRODUCTO = '" + Producto + "';");
					e.printStackTrace();
				}
			}
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}
	/**
	 * A safe way to get an instance of the Camera object.
	 *
	 * @return Camera
	 */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}
		return c; // returns null if camera is unavailable
	}

	/**
	 * Create a file Uri for saving an image or video
	 */
	@SuppressWarnings("unused")
	private static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	/**
	 * Create a File for saving an image or video
	 */
	@SuppressLint("SimpleDateFormat")
	private static File getOutputMediaFile(int type) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"TPVAndroid");
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.
		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(TAG, "Error al crear el directorio de imagenes");
				return null;
			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ transformaCadena(bundle.getString("Producto")) + ".jpg");

		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + timeStamp + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}

	@Override
	// Configuracion del boton atr?s
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		finish();
		Log.d(TAG, "Adios");
		android.os.Process.killProcess(android.os.Process.myPid());// Cerramos
																	// el
																	// proceso
																	// en el SO
		return true;
	}
	@SuppressLint("DefaultLocale")
	static String transformaCadena(String producto) {
		producto = producto.toLowerCase();
		producto = producto.replace("/", "_");
		producto = producto.replace(" ", "_");
		producto = producto.replace("�", "n");
		producto = producto.replace("+", "_");
		Log.i("ImagenesProductos", producto);
		return producto;
	}

}
