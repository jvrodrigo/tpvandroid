package com.globales.tpvandroid.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;
import com.globales.tpvandroid.models.Configuracion;
import com.globales.tpvandroid.models.Encrypter;

//import es.globales.encryptor.IGEHOS_Cipher;

@SuppressWarnings("deprecation")
@SuppressLint("HandlerLeak")
public class MainActivity extends Activity {
	EditText nombre;
	EditText password;
	Conexion conex;
	String num;
	String selectedItem = "";
	Button configBtn;
	ImageView imageView;
	String 	RutaLogoMain;
	
	Handler handlerCam = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listacam = (ArrayList<String>) bundle.get("Camareros");
			SeleccCamareros(listacam);
		}
	};


	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};


	Handler loghandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			if (bundle.getBoolean("correcto")){
				ComprobarTablet(leerNumDispositivo());
			} else
				Toast.makeText(getApplicationContext(), 
						"El Usuario o contrase�a no son correctos",
						Toast.LENGTH_LONG).show();			
		}
	};

	Handler bloqueohandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Toast.makeText(getApplicationContext(), "La caja ha sido bloqueada :(", Toast.LENGTH_LONG).show();			
		}
	};
	Handler desbloqueohandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Toast.makeText(getApplicationContext(), "La caja ha sido desbloqueada ;)", Toast.LENGTH_LONG).show();			
		}
	};
	@SuppressLint("HandlerLeak")
	Handler disphandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();

			if (bundle.getBoolean("correcto")){
				if (bundle.getBoolean("bloqueado")){
				Toast.makeText(getApplicationContext(), 
						"La caja est� bloqueada en estos momentos",
							Toast.LENGTH_LONG).show();		
				} else {
					MostrarZonas();
				}
			} else
				Toast.makeText(getApplicationContext(), 
						"El c�digo de activaci�n del dispositivo es incorrecto",
						Toast.LENGTH_LONG).show();			
		}
	};

	


	@Override
	protected void onResume() {
		super.onResume();
		this.onCreate(null);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		nombre = (EditText) findViewById(R.id.editTextNombre);
		password = (EditText) findViewById(R.id.editTextPass);
		conex = new Conexion(this);
		SharedPreferences prefe = this.getSharedPreferences("datos",Context.MODE_PRIVATE);
		num = prefe.getString("NumCaja","");
		configBtn = (Button) findViewById(R.id.botonconf);
		configBtn.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				configurar(v);
				return false;
			}
		});
		RutaLogoMain= prefe.getString("RutaLogo","");
		
		/****************************************************/
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true; 
		Display display=getWindowManager().getDefaultDisplay();
		Point size=new Point();
		display.getSize(size);
		
		//Imagenes en portada
		
		imageView = (ImageView) findViewById(R.id.layout_imagen_view_main);
		try {
			Log.i("penetreste0",RutaLogoMain);
			if(!RutaLogoMain.equals(""))
			{
				imageView.setImageBitmap( decodeSampledBitmapFromResource(RutaLogoMain,size.x, 100));
			 
 
				 
			}else{
				Log.i("penetreste2","2");
				String uri = "@drawable/logo_empresa";
				int imageResource = getResources().getIdentifier(uri, "drawable",getPackageName());
				Drawable res = getResources().getDrawable(imageResource);
				imageView.setImageDrawable(res);
			}
		}catch(Exception e)
		{
			Log.i("penetreste3","3");
			e.printStackTrace();
		}
	}
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;

	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }

	    return inSampleSize;
	}

		public static Bitmap decodeSampledBitmapFromResource(String RutaLogoMain,  int reqWidth, int reqHeight) {

		    // First decode with inJustDecodeBounds=true to check dimensions
		    final BitmapFactory.Options options = new BitmapFactory.Options();
		    options.inJustDecodeBounds = true;
		    BitmapFactory.decodeFile(RutaLogoMain, options);

		    // Calculate inSampleSize
		    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		    // Decode bitmap with inSampleSize set
		    options.inJustDecodeBounds = false;
		    return BitmapFactory.decodeFile(RutaLogoMain, options);
		}

	public void Login(View view) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					String name = nombre.getText().toString();
					String pass = password.getText().toString();

					Connection conn = conex.Conectar();
					String stsql = "SELECT USUARIO FROM empleados WHERE (USUARIO = '" +
							name + "' AND PINCODE = '" + pass + "')";
					Statement st = conn.createStatement();
					ResultSet res = st.executeQuery(stsql); 
					Boolean correcto = false;

					if (res.first()){ //NOMBRE Y PASS CORRECTO
						correcto = true;
					} 				
					conn.close();
					Message msg = loghandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putBoolean("correcto",correcto);
					msg.setData(bundle);
					loghandler.sendMessage(msg);

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}; 
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}


	public void ListarCamareros(View view) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {      
					Connection conn = conex.Conectar();
					String stsql = "Select Usuario FROM empleados";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					ArrayList<String> listaCamar = new ArrayList<String>();
					while (rs.next()){
						listaCamar.add(rs.getString(1));
					}
					Message msg = handlerCam.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("Camareros", listaCamar);
					msg.setData(bundle);  		             		
					handlerCam.sendMessage(msg);
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}


	private void SeleccCamareros(ArrayList<String> listacam) {
		if (listacam.size()!=0){
			final String[] items = listacam.toArray(new String[listacam.size()]);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Seleccione su nombre");
			selectedItem = items[0];
			builder.setCancelable(true);

			builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					selectedItem = items[which];
				}
			});

			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					nombre.setText(selectedItem);
				}
			});
			builder.show();		
		}
	}


	public void MostrarZonas() {
		Intent i = new Intent(this, ZonasActivity.class);
		i.putExtra("usuario", nombre.getText().toString());
		startActivity(i);
	}


	public void configurar(View view) {
		Intent i = new Intent(this, Configuracion.class);
		startActivity(i);
	}


	public String leerNumDispositivo() {
		TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		String uid = tManager.getDeviceId();
		return uid;
	}


	private void ComprobarTablet(final String uid) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					final CountDownLatch latchcoment = new CountDownLatch(1);
					String act = SacarCodigoAct (uid,latchcoment);
					latchcoment.await();
					//String act = "";
					
					SharedPreferences prefe = getSharedPreferences("datos",
							Context.MODE_PRIVATE);
					String numeroCaja = prefe.getString("NumCaja", "");

					String  stsql = "SELECT BLOQUEO, SERIAL " +
							"FROM tpvs " +
							"WHERE ACTIVACION = '" + act.trim() + "' AND NUM_CAJA = '" + numeroCaja + "';";
					
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					
					Boolean correcto = false;
					Boolean bloqueado = false;

					if (rs.next()){ //CODIGO CORRECTO
						Log.i("ACTIVACION", numeroCaja + "  ---  " + act.trim() + " -> " + 	rs.getString("SERIAL"));
						correcto = true;

						if (rs.getString(1).equals("0")){
							// No est� bloqueado
							bloqueado = false;
						} else {
							//lo desbloqueamos
							stsql = "UPDATE tpvs SET BLOQUEO = 0 WHERE NUM_CAJA = '" + num + "'";
							st.executeUpdate(stsql);
							desbloqueohandler.sendMessage(desbloqueohandler.obtainMessage());
						}
					}else{ // CODIGO INCORRECTO -> se bloquea la caja
						stsql = "UPDATE tpvs SET BLOQUEO = 1 WHERE NUM_CAJA = '" + num + "'";
						st.executeUpdate(stsql);
						bloqueohandler.sendMessage(bloqueohandler.obtainMessage());
					}
                    
					conn.close();
					Message msg = disphandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putBoolean("correcto",correcto);
					bundle.putBoolean("bloqueado",bloqueado);
					msg.setData(bundle);
					disphandler.sendMessage(msg);
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}

			private String SacarCodigoAct(String uid, CountDownLatch latchcoment) {
				/*try{
					IGEHOS_Cipher cipher = new IGEHOS_Cipher();
					//seguridad.CIPHER cipher = new seguridad.CIPHER();
//					 IGEHOS_Cipher cipher = new IGEHOS_Cipher();
					String aCifrar = uid;
					Log.i("Cadena original: ", aCifrar);
					String cifrado = cipher.cifrar(aCifrar);
					Log.i("Cadena cifrada: ", cifrado);
					return cifrado;
					//return "";
				}catch(Exception e){
					e.printStackTrace();
					return "";
				}
				*/
				
				try {
					Encrypter enc = new Encrypter ("123456789012345678901234","12345678");
					String resultado = enc.encryptText(uid);
					latchcoment.countDown();
					return resultado;
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
					return "";
				}
			}; 
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}


	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}
}