package com.globales.tpvandroid.activities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.dao.Conexion;
import com.globales.tpvandroid.models.Mesa;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RelativeLayout;

@SuppressLint("HandlerLeak")
public class MesasActivity extends Activity {
	Conexion conex;
	String sala;
	String usuario;
	String tipoPedido;
	List<Mesa> ListaMesas;
	java.util.Locale loc = new java.util.Locale("es_ES");

	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};


	Handler handlerOcup = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listaOcup = (ArrayList<String>) bundle.get("Ocupados");
			ArrayList<String> listaPend = (ArrayList<String>) bundle.get("Pendientes");
			DibujarMesas(listaOcup,listaPend);
		}
	};


	@Override
	protected void onResume() {
		super.onResume();
		this.onCreate(null);
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mesas);

		conex = new Conexion(this);
		Bundle bundle = getIntent().getExtras();
		String[] todasmesas = bundle.getString("mesas").split("end");
		sala = bundle.getString("sala");
		usuario = bundle.getString("usuario");
		tipoPedido = bundle.getString("tipoPedido");

		setTitle(" " + sala);

		ListaMesas = new ArrayList<Mesa>();

		for(int j = 0; j<todasmesas.length;j++) {
			String[] unamesa = todasmesas[j].split("\n  ");
			if (esMesa(unamesa[0])){

				Mesa mesa= new Mesa();
				for(int i = 0; i<unamesa.length;i++) {

					String[] keyValue = unamesa[i].split("=");
					if (keyValue.length == 2){

						String clave = keyValue[0].trim().toUpperCase(loc).toString();
						String valor = keyValue[1].toString().trim();

						if (clave.equals("LEFT")){
							mesa.setLeft(Integer.valueOf(valor));
						} else if (clave.equals("TOP")){
							mesa.setTop(Integer.valueOf(valor));
						} else if (clave.equals("WIDTH")){
							mesa.setAncho(Integer.valueOf(valor));
						} else if (clave.equals("HEIGHT")){ 
							mesa.setAlto(Integer.valueOf(valor));
						} else if (clave.equals("TARIFA")){
							mesa.setTarifa(valor);
						} else if (clave.equals("ID_MESA")){
							mesa.setId(valor);
						}	
					}
				}
				ListaMesas.add(mesa);
			}
		}
		VerMesasOcupadas();
	}


	public void VerMesasOcupadas() {
		Runnable runnable = new Runnable() {
			public void run() {
				try {  
					Connection conn = conex.Conectar();
					String stsql = "SELECT MESA,ESTADO FROM pedidos_mesas WHERE SALA = '"+ sala + "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					List<String> ListaOcup = new ArrayList<String>();
					List<String> ListaPend = new ArrayList<String>();


					while (rs.next()){
						if (rs.getString(2).equals("PEND. COBRO")){
							ListaPend.add(rs.getString(1));							
						} else {
							ListaOcup.add(rs.getString(1));
						}
					}

					Message msg = handlerOcup.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("Ocupados", (ArrayList<String>) ListaOcup);
					bundle.putStringArrayList("Pendientes", (ArrayList<String>) ListaPend);
					msg.setData(bundle);  		             		
					handlerOcup.sendMessage(msg);

					conn.close();

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();   	
	}


	public void DibujarMesas(ArrayList<String> listaOcupados, ArrayList<String> listaPend) {
		
		Display display = getWindowManager().getDefaultDisplay();
		
		// auxiliar medida de la pantalla
		Point[] posicion = new Point[ListaMesas.size()];
		int ancho;
		ancho = 0;
		for (int i=0; i < ListaMesas.size(); i++){
			posicion[i] = new Point();
			posicion[i].x = (int) (ListaMesas.get(i).getLeft());
			posicion[i].y = (int) (ListaMesas.get(i).getTop());			
			ancho = (int) (ListaMesas.get(i).getAncho());
			//rel_btnAux.height = (int) (ListaMesas.get(i).getAlto());
		}
		Point temp = new Point();
		for (int i=0; i < posicion.length; i++){
			for(int j=0 ; j < posicion.length - 1; j++){
				if (posicion[j].x > posicion[j+1].x){
					temp = posicion[j];
					posicion[j] = posicion[j+1];
					posicion[j+1] = temp;	
				}
			}
		}
		int margen;
		if(posicion[0] != null){
			margen = posicion[0].x;
		}else{
			margen=0;
		}
	
		RelativeLayout layout = (RelativeLayout)findViewById(R.id.MesasLayout);
		
		Point size = new Point();
		display.getSize(size);
		Double ajuste = (double) ((double) size.x / (double)( posicion[posicion.length - 1].x + (ancho*(1.2))) );
		
		String log = "X -> " +  size.x + "\nY -> " + size.y + "\nAjuste -> " + ajuste;
		Log.i("Tamaño de la pantalla pixeles",log);
		
		Log.i("Tamaño de las mesas",String.valueOf(posicion[posicion.length - 1].x));
		
		for (int i=0; i<ListaMesas.size(); i++){
			final Button btn = new Button(this);
			RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

			rel_btn.leftMargin = (int) ((ListaMesas.get(i).getLeft() - margen) * (ajuste * (0.85)));
			rel_btn.topMargin = (int) (ListaMesas.get(i).getTop()  * ajuste * (0.85));
			rel_btn.width = (int) (ListaMesas.get(i).getAncho() * ajuste * (1.1));
			rel_btn.height = (int) (ListaMesas.get(i).getAlto() * ajuste * (1.1));
			
			btn.setText(ListaMesas.get(i).getId());
			btn.setTextSize((int) (ajuste * 10));
			final String tar = ListaMesas.get(i).getTarifa();

			if (listaOcupados.contains(ListaMesas.get(i).getId())) {
				//verde
				
				btn.getBackground().setColorFilter(0xFF7DFF23,PorterDuff.Mode.MULTIPLY);
				btn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						final String[] items = {"Cerrar Mesa", "Continuar Añadiendo", "Cancelar"};
						AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
						alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
						alertDialog.setTitle("Que tarea desea realizar").setItems(items, new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int item) {
			            	Log.i("Mesa", "Opcion elegida: " + items[item] + " " + item);
			            	switch(item){
			            	case 0:  //Cerrar Mesa --> 0
			            	{
			            		irAComanda(btn.getText().toString(),tar,items[item],item);
			            		break;
			            	}
			            	case 1: //Continuar a�adiendo --> 1
			            	{
			            		irAComanda(btn.getText().toString(),tar,items[item],item);
			            		break;
			            	}
			            	case 2:
			            	{
			            		dialog.cancel();
			            		 break;
			            	 
			            	}
			            	}
			            }
						});
						alertDialog.show();
				}	
				}); 
			} else if(listaPend.contains(ListaMesas.get(i).getId())) {
				//morado
				btn.getBackground().setColorFilter(0xFFD19FE8,PorterDuff.Mode.MULTIPLY);
				btn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						irAComanda(btn.getText().toString(),tar,"Cobrar",3);
					}
				});	
			} else {
				//azul
				Log.i("Mesa", "Opcion elegida");
				btn.getBackground().setColorFilter(0xFF9BDDFF,PorterDuff.Mode.MULTIPLY);
				btn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
				
				   irAComanda(btn.getText().toString(),tar,"Continuar Añadiendo",1);
					}
				});	
			}	
			btn.setLayoutParams(rel_btn);
			layout.addView(btn);
		}
		
		
	}


	private void irAComanda(String idmesa, String tarif, String item1, int item2) {
		Intent i = new Intent(this, PedidosActivity.class);
		i.putExtra("idmesa", idmesa);
		i.putExtra("usuario", usuario);
		i.putExtra("sala", sala);
		i.putExtra("tarifa", tarif);
		i.putExtra("tipoPedido",tipoPedido);
		i.putExtra("estado",item1);
		i.putExtra("control",item2);
		startActivity(i);
	}


	private boolean esMesa(String cadena) {
		//Comprobamos que es una Mesa y no otro objeto (texto, panel, etc)
		String[] aux = cadena.split(":");
		if (aux.length == 2) {
			return aux[1].trim().toUpperCase(loc).toString().equals("TSALA_MESA");
		} else {
			return false;
		}
	}


	public void refrescar(View vista) {
		onResume();
	}


	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
		.setMessage(mens)
		.setCancelable(false)
		.setNegativeButton("Cerrar",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

}
