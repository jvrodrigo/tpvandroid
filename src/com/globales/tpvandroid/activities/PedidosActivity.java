package com.globales.tpvandroid.activities;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.adapters.ComandaAdapter;
import com.globales.tpvandroid.adapters.ExpandableListAdapter;
import com.globales.tpvandroid.dao.Conexion;
import com.globales.tpvandroid.models.Comanda;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.ListView;
 

@SuppressLint("HandlerLeak")
public class PedidosActivity extends Activity implements TextWatcher {
	String usuario;
	String sala;
	String idmesa;
	String tarifa;
	String numeroCaja;
	String tipoPedido;
	String TipoMesa;
	String prod1;
	boolean flag1 = false;
	int TipoControl;
	EditText NumCom;
	TextView preciototal;
	DecimalFormat decim = new DecimalFormat("0.00");
	TextView PrecioaPagar;
	TextView Cambio;
	EditText Pago;
	EditText Com;
	Locale region = new Locale("es", "ES");
	CheckBox ReImprimir;
	Button boton_pedidos_comportamiento;
	ListView lvComanda;
	RadioGroup rTipo;
	RadioButton rBotTipo;

	ArrayList<String> listFamilia;
	HashMap<String, List<String>> listProductos;
	ArrayList<String> listVariedad;
	List<Comanda> ListaPedidos = new ArrayList<Comanda>();
	ComandaAdapter adapter;

	// private ArrayAdapter<String> adaptercom;

	Conexion conex;
	private String selectedItem = "";
	double precioselectedItem;
	final CountDownLatch latchinicio = new CountDownLatch(3);
	final CountDownLatch latchcoment = new CountDownLatch(1);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pedidos);

		try {

			conex = new Conexion(this);
			NumCom = (EditText) findViewById(R.id.et_numCom);
			preciototal = (TextView) findViewById(R.id.text_precio);

			SharedPreferences prefe = getSharedPreferences("datos",
					Context.MODE_PRIVATE);
			numeroCaja = prefe.getString("NumCaja", "");

			rTipo = (RadioGroup) findViewById(R.id.rg_Tipo);

			Bundle bundle = getIntent().getExtras();
			usuario = bundle.getString("usuario");
			sala = bundle.getString("sala");
			idmesa = bundle.getString("idmesa");
			tarifa = bundle.getString("tarifa");
			tipoPedido = bundle.getString("tipoPedido");
			TipoMesa = bundle.getString("estado");
			TipoControl = bundle.getInt("control");

			setTitle(" Pedido de la Mesa " + idmesa);

			adapter = new ComandaAdapter(this, R.layout.comanda_lv,
					ListaPedidos);
			lvComanda = (ListView) findViewById(R.id.lv_Comanda);
			lvComanda.setAdapter(adapter);

			boton_pedidos_comportamiento = (Button) findViewById(R.id.boton_pedidos_comportamiento);

			ObtenComensales();
			ObtenPedidoAnterior();
			ObtenDatos_ELV();
			ComportamientoBoton();
			latchinicio.await();
			ActualizaPrecio();

			Button BotonComent = (Button) findViewById(R.id.btn_coment);
			BotonComent.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					publicaComent(v);
				}

			});

		} catch (Exception e) {
			Message msg = noconhandler.obtainMessage();
			Bundle bundle2 = new Bundle();
			bundle2.putString("error", e.getMessage());
			msg.setData(bundle2);
			noconhandler.sendMessage(msg);
		}
	}

	Handler nopermhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Toast.makeText(getApplicationContext(),
					"No tiene permisos de cobro", Toast.LENGTH_LONG).show();
		}
	};

	Handler notickethandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Toast.makeText(getApplicationContext(),
					"No se ha sacado ticket, cierre la mesa antes",
					Toast.LENGTH_LONG).show();
		}
	};

	Handler noconhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			MensajeError(bundle.getString("error"));
		}
	};

	Handler pagohandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			AbrirCobrador(bundle.getString("precio"));
		}
	};

	Handler sockethandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();

			Toast.makeText(
					getApplicationContext(),
					"No se ha podido establecer conexion con la impresora de"
							+ bundle.getString("error"), Toast.LENGTH_LONG)
					.show();
		}
	};

	Handler handlerELV = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listaTodos = (ArrayList<String>) bundle
					.get("Productos");
			Crear_ELV(listaTodos);
		}
	};

	Handler handlerEleccion = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			String producto = bundle.getString("Prod");
			if ((Boolean) bundle.get("necesitaPvP")) {
				IntroducePrecio(producto);
			} else {
				ArrayList<String> listavar = (ArrayList<String>) bundle
						.get("Variedad");
				ArrayList<String> listapun = (ArrayList<String>) bundle
						.get("Punto");
				ArrayList<String> listaprecios = (ArrayList<String>) bundle
						.get("Precio");
				SeleccionaVariedad(producto, listavar, listapun, listaprecios);
			}
		}
	};

	Handler handlerGrupo = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listagru = (ArrayList<String>) bundle
					.get("DelGrupo");
			SeleccionaDelGrupo(listagru);
		}
	};

	Handler handlerMenu = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listamenu = (ArrayList<String>) bundle
					.get("Menu");
			SeleccionaMenu(listamenu);
		}
	};

	/* MODIFICADO */

	Handler handlerCambio = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			ArrayList<String> listamenu = (ArrayList<String>) bundle
					.get("Menu");
			SeleccionaMenu(listamenu);
		}
	};

	private void ComportamientoBoton() {

		Runnable runnable = new Runnable() {

			public void run() {
				try {
					Log.i("Mesa -> ", String.valueOf(TipoControl));
					Button boton_pedidos_comportamiento = (Button) findViewById(R.id.boton_pedidos_comportamiento);
					if (TipoMesa.equals("Cerrar Mesa") || TipoControl == (0)) // Cerrar
																				// Mesa
					{
						boton_pedidos_comportamiento.setText("Cerrar Mesa");
						boton_pedidos_comportamiento
								.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										CerrarMesa(v);
									}
								});
					} else if (TipoMesa.equals("Continuar Añadiendo")
							|| TipoControl == (1)) // Confirmar
					{
						Log.i("Mesa -> ", "azulllllÇ");
						boton_pedidos_comportamiento.setText("Confirmar");
						boton_pedidos_comportamiento
								.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										confirmar(v);
									}
								});

					} else if (TipoMesa.equals("Cobrar") || TipoControl == (3)) // Cobrar
					{
						boton_pedidos_comportamiento.setText("Cobrar");
						boton_pedidos_comportamiento
								.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										cobrar();
									}
								});
					}
				} catch (Exception e) {
					Log.i("PedidosActivity", e.toString());
				}

			}
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	private void IntroducePrecio(final String producto) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setMessage("Introduzca el precio");
		alertDialog.setTitle("Introducir el precio de " + producto);
		alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		alertDialog.setCancelable(false);

		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		input.requestFocus();

		alertDialog.setView(input);

		alertDialog.setPositiveButton("Continuar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String pr = input.getText().toString();
						if (pr == "") {
							pr = "0";
						}
						anadirPedido(producto, "", "", Double.parseDouble(pr),
								true);
						dialog.cancel();
					}
				});
		alertDialog.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		alertDialog.show();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	private void ActualizaPrecio() {
		double total = 0;
		for (int i = 0; i < ListaPedidos.size(); i++) {
			total = total
					+ (ListaPedidos.get(i).getCantidad() * ListaPedidos.get(i)
							.getPreciounidad());
		}
		preciototal.setText(decim.format(total));
		adapter.notifyDataSetChanged();
	}

	/*
	 * private void ObtenComentPredef(final String[] predef) { Runnable runnable
	 * = new Runnable() {
	 * 
	 * @SuppressWarnings("unchecked") public void run() { try { Connection conn
	 * = conex.Conectar(); String stsql =
	 * "SELECT COMENTARIO FROM comentarios_cocina "; Statement st =
	 * conn.createStatement(); ResultSet rs = st.executeQuery(stsql);
	 * ArrayList<String> lista_predef = new ArrayList<String>(
	 * Arrays.asList(predef)); while (rs.next()) {
	 * lista_predef.add(rs.getString(1)); } coment_predef = (ArrayList<String>)
	 * lista_predef.clone(); latchcoment.countDown(); conn.close(); } catch
	 * (Exception e) { Message msg = noconhandler.obtainMessage(); Bundle bundle
	 * = new Bundle(); bundle.putString("error", e.getMessage());
	 * msg.setData(bundle); noconhandler.sendMessage(msg); } }; }; Thread
	 * sqlThread = new Thread(runnable); sqlThread.start(); }
	 */
	private void ObtenComensales() {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT COMENSALES FROM pedidos_mesas "
							+ "WHERE MESA = " + idmesa + " AND SALA = '" + sala
							+ "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);

					if (rs.next()) {
						NumCom.setText(rs.getString(1));
					}

					conn.close();
					latchinicio.countDown();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}
	private boolean fixProductosCocina(int id_lin_ticket){
		Connection conn;
		try {
			conn = conex.Conectar();
			String stsql = "UPDATE pedidos_mesas_line SET COMANDA_IMPRESA = 1 "
					+ "WHERE ID_LIN_TICKET = "
					+ id_lin_ticket + ";";
			Statement st = conn.createStatement();
			st.executeUpdate(stsql);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	private void ObtenPedidoAnterior() {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT CODIGO, CANTIDAD, DESCRIPCION, TIPO_L, TIPO, COMANDA_IMPRESA, PVP_UNIDAD_SD, COCINA, ID_LIN_TICKET "
							+ "FROM pedidos_mesas_line "
							+ "WHERE MESA = "
							+ idmesa + " AND SALA = '" + sala + "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);

					while (rs.next()) {
						// FIX para actualizar los productos de cocina ya impresos que aparecen como no impresos en pedidos_mesas_line
						String impresa = rs.getString(6);
						String escocina = rs.getString(8);
						int id_lin_ticket = rs.getInt(9);
						Log.i("PedidosActivity", "Fix Cocina " + impresa + " - " + escocina + " - " + id_lin_ticket);
						if (impresa.compareTo("0") == 0 &  escocina.compareTo("1") == 0){
							Log.i("PedidosActivityCoccina", rs.getString(3));
							fixProductosCocina(id_lin_ticket);
							
						}
						// FIN FIX
						
						String Tipo = rs.getString(5);
						if (Tipo == null) {
							Tipo = "0";
						}

						if (rs.getString(4).equals("PRODUCTO")) {
							Comanda com = new Comanda(rs.getString(1));
							com.setCantidad(Integer.parseInt(rs.getString(2)));
							com.setTipo(Tipo);
							String variedad = (rs.getString(3).replace(
									rs.getString(1), "")).trim();
							com.setVariedad(variedad);
							com.setImpresa(impresa != null);
							com.setPreciounidad(rs.getString(7));
							// vemos si tiene punto
							if (rs.next()) {
								if (rs.getString(4).equals("INGREDIENT")) {
									com.setPunto(rs.getString(1));
								} else {
									rs.previous();
								}
							}

							ListaPedidos.add(com);
						} else if (rs.getString(4).equals("COMENTARIO")) {
							Comanda coment = new Comanda(rs.getString(3));
							coment.setComentario(true);
							coment.setTipo(Tipo);
							coment.setImpresa(impresa != null);
							ListaPedidos.add(coment);
						}
					}
					latchinicio.countDown();
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	public void ObtenDatos_ELV() {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					String aux = "AUX" + sala + idmesa;
					Connection conn = conex.Conectar();
					Statement st = conn.createStatement();
					String stsql;
					stsql = "DROP TABLE IF EXISTS " + aux;
					st.executeUpdate(stsql);
					// FIX VER LOS PRODUCTOS SEGUN LA SALA EN LA QUE SE ENCUENTRE
					// CUIDADO: LA SALA VA RELACIONADA CON EL NUMERO DE TPV, aqui no se respeta esta norma
					/*stsql = "CREATE TABLE " + aux + " AS " + "SELECT FAMILIA "
							+ "FROM familias_carta f " + "WHERE carta = '"+ sala  + "' AND f.CARTA IN ( "
							+ "SELECT CARTA " + "FROM tpvs t "
							+ "WHERE t.NUM_CAJA = '" + numeroCaja + "')";*/
					
					//TABLA AUXILIAR PRODUCTOS/FAMILIAS
					stsql = "CREATE TABLE " + aux + " AS " + "SELECT p.PRODUCTO,p.FAMILIA "
							+ "FROM productos p WHERE p.FAMILIA IN( Select FAMILIA FROM familias_carta WHERE carta='" + sala + "')";  
					st.executeUpdate(stsql);
					//AÑADIMOS A LA TABLA AUXILIAR LOS PRODUCTOS CON SUS FAMILIAS SECUANDARIAS
					for(int i=1;i<7;i++)
					{
						stsql="SELECT PRODUCTO,FAMILIA_ADD"+i+" FROM productos WHERE FAMILIA_ADD"+i+" IS NOT NULL";
						ResultSet rs = st.executeQuery(stsql);
						while(rs.next())
						{
							stsql="INSERT INTO " + aux + " (PRODUCTO,FAMILIA) VALUES ('" + rs.getString(1) + "','" + rs.getString(2) +"');";
							try{
							Statement statem = conn.createStatement();
							statem.executeUpdate(stsql);
			
							}catch(Exception e)
							{
								e.printStackTrace();	
							}
						} 
					}
					//AÑADIMOS A LA TABLA AUXILIAR LOS PRODUCTOS CON SUS GRUPOS
					//GRUPO_TPV
					stsql="SELECT PRODUCTO,GRUPO_TPV FROM productos WHERE GRUPO_TPV IS NOT NULL";
					ResultSet rs = st.executeQuery(stsql);
					while(rs.next())
					{
						stsql="INSERT INTO " + aux + " (PRODUCTO,FAMILIA) VALUES ('" + rs.getString(1) + "','" + rs.getString(2) +"');";
						try{
						Statement statem = conn.createStatement();
						statem.executeUpdate(stsql);
		
						}catch(Exception e)
						{
							e.printStackTrace();	
						}
					}
					//GRUPO_TPV[2-6]
					for(int i=2;i<7;i++)
					{
						stsql="SELECT PRODUCTO,GRUPO_TPV"+i+" FROM productos WHERE GRUPO_TPV"+i+" IS NOT NULL";
						ResultSet rs1 = st.executeQuery(stsql);
						while(rs1.next())
						{
					
							stsql="INSERT INTO " + aux + " (PRODUCTO,FAMILIA) VALUES ('" + rs1.getString(1) + "','" + rs1.getString(2) +"');";
			
							try{
							Statement statem = conn.createStatement();
							statem.executeUpdate(stsql);
			
							}catch(Exception e)
							{
								e.printStackTrace();					}
						} 
					}
					
					stsql="SELECT PRODUCTO,FAMILIA FROM " + aux;
					ResultSet rsfinal = st.executeQuery(stsql);
					ArrayList<String> listaProd = new ArrayList<String>();
					while (rsfinal.next()) {
						listaProd.add(rsfinal.getString(1));
						listaProd.add(rsfinal.getString(2));
					}

					//stsql = "DROP TABLE " + aux;
					//st.executeUpdate(stsql);

					Message msg = handlerELV.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("Productos", listaProd);
					msg.setData(bundle);
					handlerELV.sendMessage(msg);
					latchinicio.countDown();
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	// Metodo para inflar el menu de las FAMILIAS -> PRODUCTOS
	private void Crear_ELV(ArrayList<String> listaTodos) {
		listFamilia = new ArrayList<String>();
		listProductos = new HashMap<String, List<String>>();

		for (int i = 0; i < listaTodos.size(); i = i + 2) {
			String prod = listaTodos.get(i);
			String fam = listaTodos.get(i + 1);
			if (!listFamilia.contains(fam)) {
				listFamilia.add(fam);
			}

			List<String> listProductosaux;
			if (listProductos.get(fam) == null) {
				listProductosaux = new ArrayList<String>();
			} else {
				listProductosaux = listProductos.get(fam);
			}
			listProductosaux.add(prod);
			listProductos.put(fam, listProductosaux);
		}
		final ExpandableListView expListView = (ExpandableListView) findViewById(R.id.elv_Productos);
		ExpandableListAdapter listAdapter = new ExpandableListAdapter(this,
				listFamilia, listProductos);
		expListView.setAdapter(listAdapter);

		expListView
				.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
					@Override
					public boolean onChildClick(ExpandableListView parent,
							View v, int groupPosition, int childPosition,
							long id) {
						String Producto = parent.getExpandableListAdapter()
								.getChild(groupPosition, childPosition)
								.toString();
						eleccion(Producto);
						adapter.notifyDataSetChanged();
						return true;
					}
				});

		expListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				int itemType = ExpandableListView.getPackedPositionType(id);
				if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
					String Producto = ((ExpandableListView) parent)
							.getExpandableListAdapter()
							.getChild(
									ExpandableListView
											.getPackedPositionGroup(id),
									(int) ExpandableListView
											.getPackedPositionChild(id))
							.toString();
					verImagenProducto(Producto);
				} else {

				}
				return true;
			}

		});

		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {
			int previousItem = -1;
			// recogemos el grupo abierto anterior si abrimos otro
			@Override
			public void onGroupExpand(int groupPosition) {
				if (groupPosition != previousItem)
					expListView.collapseGroup(previousItem);
				previousItem = groupPosition;
			}
		});
	}

	private void verImagenProducto(final String Producto) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(Producto);
		builder.setPositiveButton("Ver imagen",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Bundle bundle = new Bundle();
						bundle.putString("Producto", Producto);
						Intent ImagenProducto = new Intent();
						ImagenProducto.putExtras(bundle);// Pasamos el bundle a
															// la siguiente
															// actividad
						ImagenProducto.setClass(PedidosActivity.this,
								ImagenesProductosActivity.class);
						startActivity(ImagenProducto); // Comienza actividad
						// Log.i("Pedidos -> ", String.valueOf(which));
					}
				});
		builder.show();
	}

	private void eleccion(final String producto) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql;
					Statement st = conn.createStatement();

					stsql = "SELECT FAMILIA FROM productos where TIPO_ESC = 'GRUPO' "
							+ "AND PRODUCTO = '" + producto + "'";
					ResultSet resg = st.executeQuery(stsql);

					if (resg.next()) {
						List<String> listagrupo = new ArrayList<String>();
						if (resg.getString(1).equals("MENUS")) {
							stsql = "SELECT PRODUCTO FROM productos WHERE GRUPO_TPV = '"
									+ producto + "' ORDER BY POSICION";
							ResultSet resgru = st.executeQuery(stsql);

							resgru.next();
							eleccion(resgru.getString(1)); // precio y titulo
							while (resgru.next()) {
								listagrupo.add(resgru.getString(1));
							}
							Message msg = handlerMenu.obtainMessage();
							Bundle bundle = new Bundle();
							bundle.putStringArrayList("Menu",
									(ArrayList<String>) listagrupo);
							msg.setData(bundle);
							handlerMenu.sendMessage(msg);

						} else {
							// Es un Grupo
							stsql = "SELECT PRODUCTO FROM productos WHERE GRUPO_TPV = '"
									+ producto + "'";
							ResultSet resgru = st.executeQuery(stsql);
							while (resgru.next()) {
								listagrupo.add(resgru.getString(1));
							}
							Message msg = handlerEleccion.obtainMessage();
							Bundle bundle = new Bundle();
							bundle.putStringArrayList("DelGrupo",
									(ArrayList<String>) listagrupo);
							msg.setData(bundle);
							handlerGrupo.sendMessage(msg);
						}
					} else {
						String pvp;
						if (tarifa.equals("1")) {
							pvp = "PVP_T";
						} else {
							pvp = "PVP" + tarifa + "_T";
						}

						stsql = "SELECT DESC_T1,DESC_T2,DESC_T3,DESC_T4,DESC_T5, "
								+ pvp
								+ "1,"
								+ pvp
								+ "2,"
								+ pvp
								+ "3,"
								+ pvp
								+ "4,"
								+ pvp
								+ "5,"
								+ "PREGUNTAR_PRECIO "
								+ "FROM productos "
								+ "WHERE PRODUCTO = '"
								+ producto + "' LIMIT 1";
						ResultSet rs = st.executeQuery(stsql);

						List<String> listavar = new ArrayList<String>();
						List<String> listpunto = new ArrayList<String>();
						List<String> listprecios = new ArrayList<String>();

						rs.next();
						boolean necesitaPrecio = true;
						if (rs.getString(11).equals("0")) {
							necesitaPrecio = false;
							String primerPrecio = rs.getString(6);
							if (primerPrecio == null) {
								primerPrecio = "0";
							}
							listprecios.add(primerPrecio);

							if (!(rs.getString(1)).equals("")) {
								listavar.add(rs.getString(1));
								if (!(rs.getString(2)).equals("")) {
									listavar.add(rs.getString(2));
									listprecios.add(rs.getString(7));
									if (!(rs.getString(3)).equals("")) {
										listavar.add(rs.getString(3));
										listprecios.add(rs.getString(8));
										if (!(rs.getString(4)).equals("")) {
											listavar.add(rs.getString(4));
											listprecios.add(rs.getString(9));
											if (!(rs.getString(5)).equals("")) {
												listavar.add(rs.getString(5));
												listprecios.add(rs
														.getString(10));
											}
										}
									}
								}
							}

							stsql = "SELECT GRUPO_INGREDIENTES "
									+ "FROM productos " + "WHERE PRODUCTO = '"
									+ producto
									+ "' AND GRUPO_INGREDIENTES IS NOT NULL";
							ResultSet res = st.executeQuery(stsql);

							if (res.first()) { // tiene punto
								stsql = "SELECT INGREDIENTE "
										+ "FROM ingredientes "
										+ "WHERE GRUPO_INGREDIENTES = "
										+ "(SELECT GRUPO_INGREDIENTES FROM productos "
										+ "WHERE PRODUCTO = '" + producto
										+ "')";
								res = st.executeQuery(stsql);
								while (res.next()) {
									listpunto.add(res.getString(1));
								}
							}
						}
						Message msg = handlerEleccion.obtainMessage();
						Bundle bundle = new Bundle();
						bundle.putString("Prod", producto);
						bundle.putStringArrayList("Variedad",
								(ArrayList<String>) listavar);
						bundle.putStringArrayList("Punto",
								(ArrayList<String>) listpunto);
						bundle.putStringArrayList("Precio",
								(ArrayList<String>) listprecios);
						bundle.putBoolean("necesitaPvP", necesitaPrecio);
						msg.setData(bundle);
						handlerEleccion.sendMessage(msg);
					}
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			}
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	private void SeleccionaDelGrupo(List<String> listagru) {
		final String[] items = listagru.toArray(new String[listagru.size()]);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Escoja");

		selectedItem = items[0];

		builder.setCancelable(false);

		builder.setSingleChoiceItems(items, 0,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						selectedItem = items[which];
					}
				});

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				eleccion(selectedItem);
			}
		});
		builder.show();
	}


	private void SeleccionaMenu(List<String> listamenu) {
		final String[] items = listamenu.toArray(new String[listamenu.size()]);
		final boolean[] itemsChecked = new boolean[items.length];
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Escoja el menu");
		builder.setMultiChoiceItems(items, itemsChecked,
				new DialogInterface.OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int which,
							boolean isChecked) {
						itemsChecked[which] = isChecked;
					}
				});

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				for (int i = 0; i < items.length; i++) {
					if (itemsChecked[i]) {
						eleccion(items[i]);
						itemsChecked[i] = false;
					}
				}
			}
		});

		builder.show();
	}

	private void SeleccionaVariedad(final String producto,
			List<String> listavar, final List<String> listapun,
			final List<String> listaprecios) {
		if (listavar.size() == 0) {
			SeleccionaElPunto(producto, "", listapun,
					Double.parseDouble(listaprecios.get(0)));
		} else {
			final String[] items = listavar
					.toArray(new String[listavar.size()]);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Elija la variedad");

			selectedItem = items[0];
			precioselectedItem = Double.parseDouble(listaprecios.get(0));

			builder.setCancelable(false);

			builder.setSingleChoiceItems(items, 0,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							selectedItem = items[which];
							precioselectedItem = Double
									.parseDouble(listaprecios.get(which));
						}
					});
			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SeleccionaElPunto(producto, selectedItem, listapun,
									precioselectedItem);
						}
					});
			builder.show();
		}
	}

	protected void SeleccionaElPunto(final String producto,
			final String variedad, List<String> listapunto, final double precio) {
		if (listapunto.size() == 0) {
			anadirPedido(producto, variedad, "", precio, false);
		} else {
			final String[] items = listapunto.toArray(new String[listapunto
					.size()]);
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Elija como lo quiere");
			selectedItem = items[0];
			builder.setCancelable(false);

			builder.setSingleChoiceItems(items, 0,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							selectedItem = items[which];
						}
					});

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							anadirPedido(producto, variedad, selectedItem,
									precio, false);
							adapter.notifyDataSetChanged();
						}
					});
			builder.show();
		}
	}

	private void anadirPedido(String producto, String variedad, String punto,
			double precio, boolean varios) {
		Boolean encontrado = false;
		String tipo;

		int selectedId = rTipo.getCheckedRadioButtonId();
		rBotTipo = (RadioButton) findViewById(selectedId);

		if (rBotTipo.getText().equals("Primero")) {
			tipo = "1";
		} else if (rBotTipo.getText().equals("Segundo")) {
			tipo = "2";
		} else if (rBotTipo.getText().equals("Postre")) {
			tipo = "3";
		} else {
			tipo = "0";
		}

		int i = 0;
		while (!encontrado && i < ListaPedidos.size()) {
			if ((ListaPedidos.get(i).getNombre().equals(producto))
					&& (ListaPedidos.get(i).getVariedad().equals(variedad))
					&& (ListaPedidos.get(i).getPunto().equals(punto))
					&& (ListaPedidos.get(i).getTipo().equals(tipo))
					&& (!ListaPedidos.get(i).isImpresa())) {
				encontrado = true;
			} else {
				i++;
			}
		}
		if (encontrado && !varios) {
			ListaPedidos.get(i).anadirUno();
		} else {
			Comanda com = new Comanda(producto);
			com.setVariedad(variedad);
			com.setPunto(punto);
			com.setTipo(tipo);
			com.setPreciounidad(precio);
			AnadirPed(com);
		}
		// adapter.notifyDataSetChanged();
		ActualizaPrecio();
	}

	public void AnadirPed(Comanda com) {
		Boolean encontrado = false;
		int posicion = 1;
		String tipo = com.getTipo();

		for (int i = 0; i < ListaPedidos.size(); i++) {
			if (ListaPedidos.get(i).getTipo().equals(tipo)) {
				encontrado = true;
				posicion = i;
			}
		}
		if (!encontrado) {
			ListaPedidos.add(com);
		} else {
			ListaPedidos.add(posicion + 1, com);
		}
		lvComanda.smoothScrollToPositionFromTop(posicion, 50);
	}

	public void boton_sumar_prod(View vista) {
		Comanda com = (Comanda) vista.getTag();
		int posicion = ListaPedidos.indexOf(com);
		if (!ListaPedidos.get(posicion).isImpresa()) {
			ListaPedidos.get(posicion).anadirUno();
		}
		ActualizaPrecio();
	}

	public void boton_restar_prod(View vista) {
		Comanda com = (Comanda) vista.getTag();
		int posicion = ListaPedidos.indexOf(com);
		if (ListaPedidos.get(posicion).getCantidad() > 1) {
			ListaPedidos.get(posicion).quitarUno();
		}
		ActualizaPrecio();
	}

	public void boton_quitar_prod(View vista) {
		Comanda com = (Comanda) vista.getTag();
		int posicion = ListaPedidos.indexOf(com);
		ListaPedidos.remove(posicion);
		ActualizaPrecio();
	}

	public void publicaComent(View vista) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setMessage("Introduzca un Comentario");
		alertDialog.setTitle("Introducir Comentario");
		alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		alertDialog.setCancelable(false);
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);

		input.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
		alertDialog.setView(input);

		alertDialog.setPositiveButton("Continuar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						Com = input;
						String tipo;
						int selectedId = rTipo.getCheckedRadioButtonId();
						rBotTipo = (RadioButton) findViewById(selectedId);
						if (rBotTipo.getText().equals("Primero")) {
							tipo = "1";
						} else if (rBotTipo.getText().equals("Segundo")) {
							tipo = "2";
						} else if (rBotTipo.getText().equals("Postre")) {
							tipo = "3";
						} else {
							tipo = "0";
						}
						Comanda coment = new Comanda(">"
								+ (Com.getText().toString()));
						coment.setComentario(true);
						coment.setTipo(tipo);

						Boolean encontrado = false;
						int posicion = 1;
						for (int i = 0; i < ListaPedidos.size(); i++) {
							if (ListaPedidos.get(i).getTipo().equals(tipo)) {
								encontrado = true;
								posicion = i;

							}
						}
						if (!encontrado) {
							ListaPedidos.add(coment);

						} else {
							ListaPedidos.add(posicion + 1, coment);
						}
						lvComanda.bringToFront();
						dialog.cancel();
					}
				});
		alertDialog.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();

	}

	/*
	 * public void confirmar(final View vista) { if
	 * ((NumCom.getText().toString()).equals("")) {
	 * 
	 * AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
	 * alertDialog .setMessage("Introduzca el número de comensales de la mesa "
	 * + idmesa); alertDialog.setTitle("Introducir número de comensales");
	 * alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	 * alertDialog.setCancelable(false);
	 * 
	 * final EditText input = new EditText(this);
	 * input.setInputType(InputType.TYPE_CLASS_NUMBER); input.requestFocus();
	 * InputMethodManager imm = (InputMethodManager)
	 * getSystemService(Context.INPUT_METHOD_SERVICE); imm.showSoftInput(input,
	 * InputMethodManager.SHOW_IMPLICIT); alertDialog.setView(input);
	 * 
	 * alertDialog.setPositiveButton("Continuar", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int which) { String com = input.getText().toString();
	 * NumCom.setText(com); confirmar(vista); dialog.cancel(); } });
	 * alertDialog.setNegativeButton("Cancelar", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int which) { dialog.cancel(); } }); alertDialog.show(); } else {
	 * AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
	 * alertDialog.setMessage("¿Desea confirmar el pedido de la mesa " + idmesa
	 * + "?"); alertDialog.setTitle("Confirmar Pedido"); //
	 * alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
	 * alertDialog.setCancelable(true);
	 * 
	 * alertDialog.setPositiveButton("Sí", new DialogInterface.OnClickListener()
	 * { public void onClick(final DialogInterface dialog, int which) { try {
	 * CountDownLatch latchconfimp = new CountDownLatch(1);
	 * Log.i("ConfirmarPedido", "Confirmando pedido");
	 * ConfirmaPedido(latchconfimp); Log.i("ConfirmarPedido",
	 * "Pedido Confirmado OK");
	 * 
	 * latchconfimp.await(); //CountDownLatch latchimp = new CountDownLatch(1);
	 * //imprime(latchimp); //Log.i("Impresion del pedido",
	 * "Pedido Imprimido OK"); //latchimp.await(); dialog.cancel(); finish();
	 * 
	 * } catch (Exception e) { Message msg = noconhandler.obtainMessage();
	 * Bundle bundle = new Bundle(); bundle.putString("error", e.getMessage());
	 * msg.setData(bundle); noconhandler.sendMessage(msg); } } });
	 * alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener()
	 * { public void onClick(DialogInterface dialog, int which) {
	 * dialog.cancel(); } }); alertDialog.show(); } }
	 */
	public void EsCocina(final CountDownLatch latchconfimp) {
		Runnable runnable = new Runnable() {
			public void run() {
				String stsql = "SELECT NOTIFICAR_COCINA FROM productos WHERE PRODUCTO = '"
						+ prod1 + "';";
				Statement st;
				try {
			
					Connection conn = conex.Conectar();
					st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					
					while (rs.next()) {
						if (rs.getString("NOTIFICAR_COCINA").equals("1")) {
							Log.i("Mesa -> ", "SI notificar");
							flag1 = true;

						}
					}
					conn.close();
					latchconfimp.countDown();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
		};

		Thread sqlThread = new Thread(runnable);
		sqlThread.start();

	}

	public void confirmar(final View vista) {
		try {

			for (int i = 0; i < ListaPedidos.size(); i++) {
				prod1 = ListaPedidos.get(i).getNombre();
				CountDownLatch latchconfimp = new CountDownLatch(1);

				EsCocina(latchconfimp);
				latchconfimp.await();
			}
			
			if (flag1 == false) { // no notificar
				String com = "0";
				NumCom.setText(com);
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(
						vista.getContext());
				
				alertDialog.setMessage("¿Desea confirmar el pedido de la mesa "
						+ idmesa + "?");
				alertDialog.setTitle("Confirmar Pedido");
				alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
				alertDialog.setCancelable(true);
				
				alertDialog.setPositiveButton("Sí",
						new DialogInterface.OnClickListener() {
							public void onClick(final DialogInterface dialog,
									int which) {
								try {
									CountDownLatch latchconfimp = new CountDownLatch(
											1);
									Log.i("ConfirmarPedido",
											"Confirmando pedido");
									ConfirmaPedido(latchconfimp);
									Log.i("ConfirmarPedido",
											"Pedido Confirmado OK");

									latchconfimp.await();
									CountDownLatch latchimp = new CountDownLatch(
											1);
									imprime(latchimp);
									Log.i("Impresion del pedido",
											"Pedido Imprimido OK");

									dialog.cancel();
									finish();

								} catch (Exception e) {
									Message msg = noconhandler.obtainMessage();
									Bundle bundle = new Bundle();
									bundle.putString("error", e.getMessage());
									msg.setData(bundle);
									noconhandler.sendMessage(msg);
								}
							}
						});
				alertDialog.setNegativeButton("No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						});
				alertDialog.show();
			} else if (flag1 == true) { // notificar
				if ((NumCom.getText().toString()).equals("")) {
					
					flag1 = false;
					
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							this);
					alertDialog
							.setMessage("Introduzca el número de comensales de la mesa "
									+ idmesa);
					alertDialog.setTitle("Introducir número de comensales");
					alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
					alertDialog.setCancelable(false);

					final EditText input = new EditText(this);
					input.setInputType(InputType.TYPE_CLASS_NUMBER);
					input.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(input, InputMethodManager.SHOW_IMPLICIT);
					alertDialog.setView(input);

					alertDialog.setPositiveButton("Continuar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									String com = input.getText().toString();
									NumCom.setText(com);
									CountDownLatch latchconfimp = new CountDownLatch(
											1);
									Log.i("ConfirmarPedido",
											"Confirmando pedido");
									ConfirmaPedido(latchconfimp);
									Log.i("ConfirmarPedido",
											"Pedido Confirmado OK");
									try {
										latchconfimp.await();
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									CountDownLatch latchimp = new CountDownLatch(
											1);
									imprime(latchimp);
									Log.i("Impresion del pedido",
											"Pedido Imprimido OK");

									dialog.cancel();
									finish();

								}
							});
					alertDialog.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();
								}
							});
					alertDialog.show();

				} else {
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							vista.getContext());
					Log.i("ConfirmarPedido", "julaii pedido");
					alertDialog
							.setMessage("¿Desea confirmar el pedido de la mesa "
									+ idmesa + "?");
					alertDialog.setTitle("Confirmar Pedido");
					alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
					alertDialog.setCancelable(true);
					Log.i("ConfirmarPedido", "julaii pedido");
					alertDialog.setPositiveButton("Sí",
							new DialogInterface.OnClickListener() {
								public void onClick(
										final DialogInterface dialog, int which) {
									try {
										CountDownLatch latchconfimp = new CountDownLatch(
												1);
										Log.i("ConfirmarPedido",
												"Confirmando pedido");
										ConfirmaPedido(latchconfimp);
										Log.i("ConfirmarPedido",
												"Pedido Confirmado OK");

										latchconfimp.await();
										CountDownLatch latchimp = new CountDownLatch(
												1);
										imprime(latchimp);
										Log.i("Impresion del pedido",
												"Pedido Imprimido OK");
										// latchimp.await();
										dialog.cancel();
										finish();

									} catch (Exception e) {
										Message msg = noconhandler
												.obtainMessage();
										Bundle bundle = new Bundle();
										bundle.putString("error",
												e.getMessage());
										msg.setData(bundle);
										noconhandler.sendMessage(msg);
									}
								}
							});
					alertDialog.setNegativeButton("No",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.cancel();
								}
							});
					alertDialog.show();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void ConfirmaPedido(final CountDownLatch latchconfimp) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					String comensales = NumCom.getText().toString();
					Connection conn = conex.Conectar();
					Statement st = conn.createStatement();
					String stsql;

					// Creamos una mesa en pedidos_mesas si no existe
					stsql = "INSERT INTO pedidos_mesas "
							+ "(MESA,SALA,USUARIO,TCK_APERTURA,TCK_CIERRE,OBS,DES_MESA,IMPORTE,COMENSALES,TIPO) "
							+ "SELECT "
							+ idmesa
							+ " ,'"
							+ sala
							+ "','"
							+ usuario
							+ "',now(),now(),'','"
							+ idmesa
							+ "',0,"
							+ comensales
							+ ",0 "
							+ "FROM dual "
							+ "WHERE NOT EXISTS (SELECT MESA,SALA FROM pedidos_mesas "
							+ "WHERE MESA = " + idmesa + " AND SALA = '" + sala
							+ "' LIMIT 1)";

					st.executeUpdate(stsql);

					// Eliminamos los registros que no han sido impresos
					// para actualizar y evitar duplicidades
					stsql = "DELETE FROM pedidos_mesas_line " + "WHERE MESA = "
							+ idmesa + " AND SALA = '" + sala
							+ "' AND COMANDA_IMPRESA IS NULL";
					st.executeUpdate(stsql);

					// Creamos el pedido en pedidos_mesas_line
					int cant;
					String prod, var, punto, tipo, precio, preciototal;

					for (int i = 0; i < ListaPedidos.size(); i++) {
						if (!ListaPedidos.get(i).isImpresa()) {
							tipo = ListaPedidos.get(i).getTipo();

							if (!ListaPedidos.get(i).isComentario()) {
								prod = ListaPedidos.get(i).getNombre();
								cant = ListaPedidos.get(i).getCantidad();
								var = ListaPedidos.get(i).getVariedad();
								punto = ListaPedidos.get(i).getPunto();
								precio = decim.format(ListaPedidos.get(i)
										.getPreciounidad());
								preciototal = decim.format(ListaPedidos.get(i)
										.getPreciounidad() * cant);
								precio = precio.replace(",", ".");
								preciototal = preciototal.replace(",", ".");

								if (var.equals("")) {
									stsql = "INSERT INTO pedidos_mesas_line "
											+ "(MESA,SALA,TIPO_L,CODIGO,DESCRIPCION,"
											+ "CANTIDAD,PRODUCTO_P,TAM,FAMILIA,"
											+ "PVP_UNIDAD_SD,IMPORTE_SD,IMPORTE,"
											+ "TARIFA,IDTARIFA,"
											+ "COCINA,COMANDA_IMPRESA,PRECIO_FIJADO,USUARIO,TIPO) "
											+ "SELECT "
											+ idmesa
											+ ",'"
											+ sala
											+ "','PRODUCTO','"
											+ prod
											+ "','"
											+ prod
											+ "',"
											+ cant
											+ ","
											+ "'"
											+ prod
											+ "',1,p.FAMILIA,"
											+ precio
											+ ","
											+ preciototal
											+ ","
											+ preciototal
											+ ", 'T"
											+ tarifa
											+ "',"
											+ tarifa
											+ ","
											+ "p.NOTIFICAR_COCINA"
											+ ","
											+ "IF(p.NOTIFICAR_COCINA = 1, 0, NULL),0,'"
											+ usuario
											+ "',"
											+ tipo
											+ " FROM productos p "
											+ "WHERE p.PRODUCTO = '"
											+ prod
											+ "'";
								} else {
									stsql = "INSERT INTO pedidos_mesas_line "
											+ "(MESA,SALA,TIPO_L,CODIGO,DESCRIPCION,"
											+ "CANTIDAD,PRODUCTO_P,TAM,DESC_TAM,FAMILIA,"
											+ "PVP_UNIDAD_SD,IMPORTE_SD,IMPORTE,"
											+ "TARIFA,IDTARIFA,"
											+ "COCINA,COMANDA_IMPRESA,PRECIO_FIJADO,USUARIO,TIPO) "
											+ "SELECT "
											+ idmesa
											+ ",'"
											+ sala
											+ "','PRODUCTO','"
											+ prod
											+ "','"
											+ prod
											+ " "
											+ var
											+ "',"
											+ cant
											+ ","
											+ "'"
											+ prod
											+ "',1,'"
											+ var
											+ "',p.FAMILIA,"
											+ precio
											+ ","
											+ preciototal
											+ ","
											+ preciototal
											+ ",'T"
											+ tarifa
											+ "',"
											+ tarifa
											+ ","
											+ "p.NOTIFICAR_COCINA"
											+ ","
											+ "IF(p.NOTIFICAR_COCINA = 1, 0, NULL)"
											+",0,'"
											+ usuario
											+ "',"
											+ tipo
											+ " FROM productos p "
											+ "WHERE p.PRODUCTO = '"
											+ prod
											+ "'";
								}
								st.executeUpdate(stsql);

								if (!punto.equals("")) {
									stsql = "INSERT INTO pedidos_mesas_line "
											+ "(MESA,SALA,TIPO_L,CODIGO,DESCRIPCION,"
											+ "CANTIDAD,PRODUCTO_P,TAM,FAMILIA,"
											+ "PVP_UNIDAD_SD,IMPORTE_SD,IMPORTE,"
											+ "TARIFA,IDTARIFA,"
											+ "COCINA,USUARIO,TIPO) "
											+ "SELECT "
											+ idmesa
											+ ",'"
											+ sala
											+ "','INGREDIENT','"
											+ punto
											+ "','"
											+ punto
											+ "',"
											+ cant
											+ ","
											+ "'"
											+ prod
											+ "',1,p.FAMILIA,"
											+ "0,0,0,"
											+ "'T"
											+ tarifa
											+ "',"
											+ tarifa
											+ ","
											+ "p.NOTIFICAR_COCINA,'"
											+ usuario
											+ "',"
											+ tipo
											+ " FROM productos p "
											+ "WHERE p.PRODUCTO = '"
											+ prod
											+ "'";
									st.executeUpdate(stsql);
								}
							} else {
								// Comentario
								stsql = "INSERT INTO pedidos_mesas_line "
										+ "(MESA,SALA,TIPO_L,"
										+ "DESCRIPCION,CANTIDAD, PVP_UNIDAD_SD,IMPORTE_SD,IMPORTE,COCINA,TIPO) "
										+ "SELECT " + idmesa + ",'" + sala
										+ "','COMENTARIO','"
										+ ListaPedidos.get(i).getNombre()
										+ "',0,0,0,0,1," + tipo;
								st.executeUpdate(stsql);
							}
						}
					}
					// importe total en pedidos_mesas
					stsql = "UPDATE pedidos_mesas "
							+ "SET COMENSALES = "
							+ comensales
							+ ", ESTADO = 'OCUPADA',IMPORTE = "
							+ "(SELECT SUM(p.IMPORTE) FROM pedidos_mesas_line p "
							+ "WHERE p.MESA = " + idmesa + " AND p.SALA = '"
							+ sala + "') " + "WHERE MESA = " + idmesa
							+ " AND SALA = '" + sala + "'";
					st.executeUpdate(stsql);

					// Insercion del pedido para disparar la rutina de impresion
					// de la comanda de BARRA
					//1º Busca impresora de destino
					stsql = "SELECT SER_IMP_DESTINO FROM tpvs WHERE NUM_CAJA = '" + numeroCaja + "';";
					ResultSet rs = st.executeQuery(stsql);
					String serImpDestino = "";
					while(rs.next()){
						serImpDestino = rs.getString("SER_IMP_DESTINO");
					}
					
					stsql = "INSERT INTO dbspool (CAJA, OPERACION, P1, P2, P3, DESTINO) VALUES ('"
							+ numeroCaja
							+ "', 'COCINA', '"
							+ sala
							+ "', '"
							+ idmesa + "' , 0, '" + serImpDestino + "');";
					st.executeUpdate(stsql);
					
					stsql = "INSERT INTO dbspool (CAJA, OPERACION, P1, P2, DESTINO) VALUES ('"
							+ numeroCaja
							+ "', 'BARRA', '"
							+ sala
							+ "', '"
							+ idmesa + "','" + serImpDestino + "');";
					st.executeUpdate(stsql);
				
					
					conn.close();
					latchconfimp.countDown();

				} catch (Exception e) {
					// Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					Log.i("ErrorConfirmar", e.getMessage());
					// msg.setData(bundle);
					// noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	public void imprime(final CountDownLatch latchimp) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					String stsql = "SELECT IMPRIMIR_SER_IMP "
							+ "FROM tpvs WHERE NUM_CAJA = '" + numeroCaja + "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					if (rs.next()) {
						String servidor = rs.getString(1);
						if (servidor == null) {
							MensajeError("No se ha inicializado el campo IMPRIMIR_SER_IMP en la Base de Datos");
						} else {
							if (servidor.equals("1")) {

								// miramos si es la primera comanda de la mesa
								String numeroComanda = "0";
								stsql = "SELECT P3 FROM dbspool "
										+ "WHERE P1 = '" + sala + "' AND P2 = "
										+ idmesa + " ORDER BY P3 DESC LIMIT 1";
								rs = st.executeQuery(stsql);
								if (rs.next()) {
									numeroComanda = rs.getString(1);
								}
								numeroComanda = Integer.toString(Integer
										.parseInt(numeroComanda) + 1);
								stsql = "INSERT INTO dbspool (CAJA,OPERACION,P1,P2,P3,DESTINO) values ("
										+ "SELECT '"
										+ numeroCaja
										+ "','COCINA','"
										+ sala
										+ "',"
										+ idmesa
										+ ",'"
										+ numeroComanda
										+ "',t.SER_IMP_DESTINO "
										+ "FROM tpvs t "
										+ "WHERE t.NUM_CAJA = '"
										+ numeroCaja
										+ "');";
								st.executeUpdate(stsql);

								stsql = "UPDATE pedidos_mesas_line SET COMANDA_IMPRESA = "
										+ numeroComanda
										+ " WHERE MESA = "
										+ idmesa
										+ " AND SALA = '"
										+ sala
										+ "' AND COMANDA_IMPRESA IS NULL";
								st.executeUpdate(stsql);
							} else {
								MensajeError("Acceso denegado al servidor de impresión");
							}
						}
					} else {
						MensajeError("No existe el campo IMPRIMIR_SER_IMP en la Base de Datos");
					}
					conn.close();
					latchimp.countDown();
				} catch (Exception e) {
					e.printStackTrace();
					/*
					 * Message msg = noconhandler.obtainMessage(); Bundle bundle
					 * = new Bundle(); bundle.putString("error",
					 * e.getMessage()); msg.setData(bundle);
					 * noconhandler.sendMessage(msg);
					 */
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	public void reimprimir(final CountDownLatch latchimp) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {

					Connection conn = conex.Conectar();
					String stsql = "SELECT IMPRIMIR_SER_IMP, SER_IMP_DESTINO "
							+ "FROM tpvs WHERE NUM_CAJA = '" + numeroCaja + "'";
					Statement st = conn.createStatement();
					ResultSet rs = st.executeQuery(stsql);
					if (rs.next()) {
						String servidor = rs.getString(1);
						if (servidor == null) {
							//Log.i("PedidosActivity", "hola1");
							//MensajeError("No se ha inicializado el campo IMPRIMIR_SER_IMP en la Base de Datos");
						} else {
							String impDestino = rs.getString("SER_IMP_DESTINO");
							Log.i("PedidosActivity","Reimprimiedo");
							Log.i("Num Caja",numeroCaja);
							Log.i("Mesa",idmesa);
							Log.i("Sala",sala);
							Log.i("Impresora destino", impDestino);
							stsql = "SELECT NUM_TICKET FROM pedidos_mesas WHERE SALA = '" + sala + "' AND MESA = " + idmesa + ";";
							Log.i("Sentencia", stsql);
							
							ResultSet rs0 = st.executeQuery(stsql);
							String result = "Nada";
							if(rs0.next()){
								result = rs0.getString(1);
								Log.i("Num ticket", result);
							}
							
							stsql = "INSERT INTO dbspool (CAJA, OPERACION, P1, DESTINO) VALUES ("
									+ numeroCaja
									+ ", 'TICKET', "
									+ result + ", '"
									+ impDestino + "');";
							
							
							Log.i("SQL -> ",stsql);
							st.executeUpdate(stsql);
						/*	stsql = "INSERT INTO dbspool (CAJA,OPERACION,P1,DESTINO)  VALUES ("
									+ "SELECT '" + numeroCaja + "','TICKET', "
									+ "(SELECT NUM_TICKET FROM pedidos_mesas "
									+ "WHERE SALA = '" + sala + "' AND MESA = '"
									+ idmesa + "')" + ",t.SER_IMP_DESTINO "
									+ "FROM tpvs t " + "WHERE t.NUM_CAJA = '"
									+ numeroCaja + "';";
							st.executeUpdate(stsql);*/
							/*if (servidor.equals("0")) {
								// Numero de ticket para poner a
								// volver a insertar en dbspool la reimpresion
								
								if (rs.next()) {
									
									// numeroComanda = rs.getString(1);
								}
								
							} else {
								Log.i("PedidosActivity", "hola4");
								// MensajeError("Acceso denegado al servidor de impresión");
							}*/
						}
					} else {
						MensajeError("No existe el campo IMPRIMIR_SER_IMP en la Base de Datos");
					}
					
					latchimp.countDown();
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
					/*
					 * Message msg = noconhandler.obtainMessage(); Bundle bundle
					 * = new Bundle(); bundle.putString("error",
					 * e.getMessage()); msg.setData(bundle);
					 * noconhandler.sendMessage(msg);
					 */
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	public void CerrarMesa(View vista) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setMessage("¿Desea cerrar el pedido de la mesa " + idmesa
				+ " ?");
		alertDialog.setTitle("Cerrar pedido de la mesa " + idmesa);
		alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
		alertDialog.setCancelable(false);
		alertDialog.setPositiveButton("Sí",
				new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, int which) {
						try {
							final CountDownLatch latchticket = new CountDownLatch(
									1);
							CrearTicket(latchticket, true);
							latchticket.await();
							dialog.cancel();
							finish();
						} catch (Exception e) {
							Message msg = noconhandler.obtainMessage();
							Bundle bundle = new Bundle();
							bundle.putString("error", e.getMessage());
							msg.setData(bundle);
							noconhandler.sendMessage(msg);
						}
					}
				});
		alertDialog.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		alertDialog.show();
	}

	public void CrearTicket(final CountDownLatch latchticket,
			final boolean cerrado) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					Statement st = conn.createStatement();
					String stsql;
					String num_ticket = "";
					boolean habiaTicket = false;

					// COMPROBAMOS SI YA SE HA SACADO UN TICKET
					stsql = "SELECT NUM_TICKET " + "FROM pedidos_mesas "
							+ "WHERE SALA = '" + sala + "' AND MESA = "
							+ idmesa;
					ResultSet rs = st.executeQuery(stsql);
					if (rs.next()) {
						num_ticket = rs.getString(1);
						if (num_ticket != null) {
							habiaTicket = true;
						}
					}

					if (habiaTicket) {
						// si hay ticket anterior lo borramos para
						// sobreescribirlo
						stsql = "DELETE FROM tickets_line "
								+ "WHERE NUM_TICKET = '" + num_ticket + "'";
						st.executeUpdate(stsql);

						// actualizamos el ticket de tickets_cab
						stsql = "UPDATE tickets_cab t, pedidos_mesas m "
								+ "SET t.IMPORTE = m.IMPORTE, t.BASE_IMP = m.IMPORTE/1.1,"
								+ "t.IVA = m.IMPORTE - m.IMPORTE/1.1, t.IMP_FP1 = m.IMPORTE,"
								+ "t.TCK_CIERRE = now(),t.FECHA = CURDATE(), t.HORA = CURTIME() "
								+ "WHERE t.NUM_TICKET = '" + num_ticket
								+ "' AND m.SALA = '" + sala + "' AND m.MESA ="
								+ idmesa;
						st.executeUpdate(stsql);

					} else {
						// CREAMOS TICKET EN TICKETS_CAB
						stsql = "INSERT INTO tickets_cab "
								+ "(NUM_TICKET,TIPO_DOC,TIPO_PEDIDO,FPAGO,NUM_CAJA,"
								+ "USUARIO,IMPORTE_SD,DESCUENTO,"
								+ "IMPORTE,TIPO_IVA,BASE_IMP,IVA,"
								+ "FECHA,HORA,SALA,MESA,"
								+ "TCK_APERTURA,TCK_CIERRE,DES_MESA,IMP_FP1,PAGADO,COMENSALES) "
								+ "SELECT "
								+ "ST_VENTA_NUM + 1,'CUENTA','"
								+ tipoPedido
								+ "','CUENTA','"
								+ numeroCaja
								+ "',"
								+ "m.USUARIO,0,0,"
								+ "m.IMPORTE,10,m.IMPORTE/1.1,m.IMPORTE - m.IMPORTE/1.1, "
								+ "CURDATE(),CURTIME(),'"
								+ sala
								+ "',"
								+ idmesa
								+ ", "
								+ "m.TCK_APERTURA,now(),m.DES_MESA,m.IMPORTE,0,m.COMENSALES "
								+ "FROM pedidos_mesas m, series s "
								+ "WHERE m.MESA = " + idmesa
								+ " AND m.SALA = '" + sala + "'";
						st.executeUpdate(stsql);

						stsql = "UPDATE series SET ST_VENTA_NUM = ST_VENTA_NUM + 1";
						st.executeUpdate(stsql);
					}

					// TICKET EN TICKET_LINE
					stsql = "INSERT INTO tickets_line "
							+ "(NUM_TICKET,"
							+ "TIPO_L,CODIGO,DESCRIPCION,CANTIDAD,PRODUCTO_P, "
							+ "TAM,DESC_TAM,FAMILIA,PROMOCION,NUM_COMANDA,PVP_UNIDAD_SD, "
							+ "IMPORTE_SD,DESCUENTO,IMPORTE,ID_AGRUP,ID_AGRUP_PROMO,TARIFA, "
							+ "IDTARIFA,COMANDA_IMPRESA,COCINA,PRECIO_FIJADO,USUARIO,"
							+ "EN_CENTRAL) "
							+ "SELECT "
							+ "(SELECT NUM_TICKET FROM tickets_cab WHERE MESA = "
							+ idmesa
							+ " AND SALA = '"
							+ sala
							+ "' ORDER BY NUM_TICKET DESC LIMIT 1), "
							+ "a.TIPO_L,a.CODIGO,a.DESCRIPCION,a.CANTIDAD,a.PRODUCTO_P, "
							+ "a.TAM,a.DESC_TAM,a.FAMILIA,a.PROMOCION,a.NUM_COMANDA,a.PVP_UNIDAD_SD, "
							+ "a.IMPORTE_SD,a.DESCUENTO,a.IMPORTE,a.ID_AGRUP,a.ID_AGRUP_PROMO,a.TARIFA, "
							+ "a.IDTARIFA,a.COMANDA_IMPRESA,a.COCINA,a.PRECIO_FIJADO,a.USUARIO, "
							+ "(SELECT EN_CENTRAL FROM tickets_cab WHERE MESA = "
							+ idmesa + " AND SALA = '" + sala
							+ "' ORDER BY NUM_TICKET DESC LIMIT 1) "
							+ "FROM pedidos_mesas_line a " + "WHERE a.MESA = "
							+ idmesa + " AND a.SALA = '" + sala + "'";
					st.executeUpdate(stsql);

					String estado = "";
					if (cerrado) {
						estado = "ESTADO = 'PEND. COBRO',";
					}
					// ACTUALIZAMOS pedidos_mesas
					stsql = "UPDATE pedidos_mesas "
							+ "SET TCK_CIERRE = now(),"
							+ estado
							+ " NUM_TICKET = (SELECT NUM_TICKET FROM tickets_cab"
							+ " WHERE MESA = " + idmesa + " AND SALA = '"
							+ sala + "'"
							+ " ORDER BY NUM_TICKET DESC LIMIT 1) "
							+ " WHERE MESA = " + idmesa + " AND SALA = '"
							+ sala + "'";
					st.executeUpdate(stsql);

					// Mandamos a imprimir el ticket
					stsql = "INSERT INTO dbspool (CAJA,OPERACION,P1,DESTINO) "
							+ "SELECT '" + numeroCaja + "','TICKET', "
							+ "(SELECT NUM_TICKET FROM pedidos_mesas "
							+ "WHERE SALA = '" + sala + "' AND MESA = "
							+ idmesa + ")" + ",t.SER_IMP_DESTINO "
							+ "FROM tpvs t " + "WHERE t.NUM_CAJA = '"
							+ numeroCaja + "'";
					st.executeUpdate(stsql);

					latchticket.countDown();
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	public void cobrar() {

		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Connection conn = conex.Conectar();
					Statement st = conn.createStatement();
					String stsql;

					// COMPROBAMOS SI SE TIENE PERMISO DE COBRO
					stsql = "SELECT PERMITE_COBROS FROM tpvs WHERE NUM_CAJA = '"
							+ numeroCaja + "'";
					ResultSet rs = st.executeQuery(stsql);
					rs.next();
					if (!rs.getString(1).equals("1")) {
						Message msg = nopermhandler.obtainMessage();
						Bundle bundle = new Bundle();
						msg.setData(bundle);
						nopermhandler.sendMessage(msg);
					} else {
						// COMPROBAMOS SI YA SE HA SACADO UN TICKET
						stsql = "SELECT ESTADO,IMPORTE FROM pedidos_mesas "
								+ "WHERE SALA = '" + sala + "' AND MESA = "
								+ idmesa;
						rs = st.executeQuery(stsql);
						if (rs.next()) {
							if (!rs.getString(1).equals("PEND. COBRO")) {
								Message msg = notickethandler.obtainMessage();
								Bundle bundle = new Bundle();
								msg.setData(bundle);
								notickethandler.sendMessage(msg);
							} else {
								String importe = rs.getString(2);
								Message msg = pagohandler.obtainMessage();
								Bundle bundle = new Bundle();
								bundle.putString("precio", importe);
								msg.setData(bundle);
								pagohandler.sendMessage(msg);
							}
						} else {
							Message msg = notickethandler.obtainMessage();
							Bundle bundle = new Bundle();
							msg.setData(bundle);
							notickethandler.sendMessage(msg);
						}
					}
					conn.close();
				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	private void AbrirCobrador(String precio) {

		View vistacobra = View.inflate(this, R.layout.cobrador, null);
		PrecioaPagar = (TextView) vistacobra.findViewById(R.id.Precio);

		Cambio = (TextView) vistacobra.findViewById(R.id.Cambio);
		RadioGroup rTipoFP = (RadioGroup) vistacobra
				.findViewById(R.id.rg_Tipo_cobrador);
		int selectFP = rTipoFP.getCheckedRadioButtonId();
		final RadioButton rBotTipoFP = (RadioButton) vistacobra
				.findViewById(selectFP);
		Pago = (EditText) vistacobra.findViewById(R.id.Pagado);
		Pago.addTextChangedListener(this);
		Pago.setOnEditorActionListener(new OE(precio, Cambio));

		// InputMethodManager imm = (InputMethodManager);
		// InputMethodManager imm = (InputMethodManager)
		// getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		// if (imm != null) { imm.showSoftInput(editText,0); }

		// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		// imm.showSoftInput(vistacobra, InputMethodManager.SHOW_IMPLICIT);
		Pago.requestFocus();
		ReImprimir = (CheckBox) vistacobra.findViewById(R.id.reImprimir);

		ReImprimir.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// is chkIos checked?
				if (((CheckBox) v).isChecked()) {
					Toast.makeText(PedidosActivity.this,
							"Reimprimiendo la Comanda", Toast.LENGTH_LONG)
							.show();
				}

			}
		});
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setCancelable(false);
		builder.setTitle("Cobrador");
		builder.setView(vistacobra);
		if (precio == null || precio == "") {
			precio = "0";
		}
		PrecioaPagar.setText(precio);

		builder.setPositiveButton("Cobrar",
				new DialogInterface.OnClickListener() {
					@SuppressLint("DefaultLocale")
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Control de excepciones
						try { // Si el cambio es cualquier cifra numerica
							float cambio = Float.valueOf(Cambio.getText()
									.toString().replace(",", "."));

							if (cambio < 0) { // Cambio menor que 0-->Falta..
								Toast.makeText(getApplicationContext(),
										"Faltan " + -1 * cambio + "euros",
										Toast.LENGTH_LONG).show();
							} else { // Si el cobro se a efectuado correctamente
								String formapago = rBotTipoFP.getText()
										.toString().toUpperCase();
								float pagado = Float.parseFloat(Pago.getText()
										.toString().replace(",", "."));
								
								if (ReImprimir.isChecked()) {
									Log.i("PedidosActivity", "ReImprimiendo");
									final CountDownLatch latchimprime = new CountDownLatch(
											1);
									reimprimir(latchimprime);
									latchimprime.await();
								}
								//Log.i("PedidosActivity", "adios");

								final CountDownLatch latchcobro = new CountDownLatch(
										1);
								ActualizarCobro(formapago, pagado, cambio,
										latchcobro);
								latchcobro.await();
								
								dialog.cancel();
								finish();
							}

						} catch (Exception e) { // Si por el contrario se ha
												// borrado dicha cantidad y la
												// cantida se pone pr defecto a
												// "CAMBIO"
							Log.i("PedidosActivity", e.toString());
							Toast.makeText(getApplicationContext(),
									"Requiere de cobro", Toast.LENGTH_LONG)
									.show();

						}
					}

				});

		builder.setNegativeButton("Cancelar",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});

		builder.create();
		builder.show();
	}

	// Introducido por textWatcher para el seguimiento del campo cobro
	@Override
	// No utilizado
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// TODO Auto-generated method stub

	}

	@Override
	// No utilizado
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	// Le utilizaremos para controlar los cambios despues de editar el campo
	// Pago
	public void afterTextChanged(Editable s) {
		// Control de excepciones
		try { // Al introducir un dato numerico cualquiera

			float change = 0;
			float Precioapagar = Float.parseFloat(PrecioaPagar.getText()
					.toString());
			float Pagado = Float.parseFloat(Pago.getText().toString());
			change = Pagado - Precioapagar;
			if (change < 0) { // Si cambio menor que cero --> Valor en negativo
								// + visualización de dicha cantidad en rojo
								// para que esto sea más intuitivo

				Cambio.setText(String.valueOf(decim.format(change)).replace(",", "."));
				Cambio.setTextColor(Color.RED);
			} else if (change > 0) { // Si cambio menor que cero --> Valor en
										// positivo + visualización de dicha
										// cantidad en verde para que esto sea
										// más intuitivo

				Cambio.setText(String.valueOf(decim.format(change)).replace(",", "."));
				Cambio.setTextColor(Color.GREEN);
			} else { // Si cambio es igual a cero --> Entonces no se le debera
						// abonar al cliente ningun cambio por lo que aparecera
						// en colo negro y con la cantidad 0.0
				Cambio.setText(String.valueOf(decim.format(change)).replace(",", "."));
				Cambio.setTextColor(Color.BLACK);
			}
		} catch (Exception e) // Si una vez introducida dicha cantidad el
								// cliente se retracta de lo pagado y quiere
								// cambiar la cantidad, el pago anterior se
								// debera borrar, indicando nuevamente "cambio"
								// por la falta del pago.
		{
			String change = "Cambio";
			Cambio.setText(change.replace(",", "."));
			Cambio.setTextColor(Color.BLACK);

		}
	}

	public void ActualizarCobro(final String formapago, final float pagado,
			final float cambio, final CountDownLatch latchcobro) {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					// Log.i("PedidosActivity","Entrando en actualizar");
					Connection conn = conex.Conectar();
					Statement st = conn.createStatement();
					String stsql;

					// Actualizar ticket_cab
					stsql = "UPDATE tickets_cab " + "SET FPAGO = '" + formapago
							+ "', CAMBIO = " + cambio + ", PAGADO = " + pagado
							+ " WHERE NUM_TICKET ="
							+ " (SELECT NUM_TICKET FROM pedidos_mesas "
							+ " WHERE SALA = '" + sala + "' AND MESA = "
							+ idmesa + ")";

					try {
						st.executeUpdate(stsql);
						// Log.i("PedidosActivity","Conexxion OK - Update1");
						// Borrar mesa de Pedidos_mesa
						// tickets line borra en cascada
						stsql = "DELETE FROM pedidos_mesas " + "WHERE SALA = '"
								+ sala + "' AND MESA = " + idmesa;
						st.executeUpdate(stsql);
						// Log.i("PedidosActivity","Conexxion OK - Update2");

						conn.close();
						latchcobro.countDown();
						// Log.i("PedidosActivity", "Conexxion OK - Saliendo");
					} catch (SQLException e) {
						Log.i("PedidosActivity", e.toString());
					}

				} catch (Exception e) {
					Message msg = noconhandler.obtainMessage();
					Bundle bundle = new Bundle();
					bundle.putString("error", e.getMessage());
					msg.setData(bundle);
					noconhandler.sendMessage(msg);
				}
			};
		};
		Thread sqlThread = new Thread(runnable);
		sqlThread.start();
	}

	class OE implements OnEditorActionListener {
		TextView Cambio;
		String Precio;

		public OE(String precio, TextView cambio) {
			Cambio = cambio;
			Precio = precio;
		}

		@Override
		public boolean onEditorAction(TextView tv, int arg1, KeyEvent arg2) {
			double pagado = Double.parseDouble(tv.getText().toString());
			double vuelta = pagado - Double.parseDouble(Precio);
			Cambio.setText(decim.format(vuelta));
			return false;
		}
	}

	public void MensajeError(String mens) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error")
				.setMessage(mens)
				.setCancelable(false)
				.setNegativeButton("Cerrar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		AlertDialog alert = builder.create();
		alert.show();
	}

}
