package com.globales.tpvandroid.adapters;

import java.text.DecimalFormat;
import java.util.List;

import com.globales.tpvandroid.R;
import com.globales.tpvandroid.models.Comanda;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
//import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class ComandaAdapter extends ArrayAdapter<Comanda> {

	private List<Comanda> items;
	private int layoutResourceId;
	private Context context;
	DecimalFormat decim = new DecimalFormat("0.00");

	public ComandaAdapter(Context context, int layoutResourceId, List<Comanda> items) {
		super(context, layoutResourceId, items);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.items = items;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ComandaHolder holder = null;

		LayoutInflater inflater = ((Activity) context).getLayoutInflater();
		row = inflater.inflate(layoutResourceId, parent, false);

		holder = new ComandaHolder();
		holder.Comanda = items.get(position);
		holder.anadirButton = (ImageButton)row.findViewById(R.id.prod_add);
		holder.anadirButton.setTag(holder.Comanda);
		holder.restarButton = (ImageButton)row.findViewById(R.id.prod_resta);
		holder.restarButton.setTag(holder.Comanda);
		holder.quitarButton = (ImageButton)row.findViewById(R.id.prod_eliminar);
		holder.quitarButton.setTag(holder.Comanda);		
		holder.producto = (TextView)row.findViewById(R.id.prod_nombre);
		holder.cantidad = (EditText)row.findViewById(R.id.prod_num);	
		holder.tipo = (TextView)row.findViewById(R.id.prod_tipo);
		holder.precio = (TextView)row.findViewById(R.id.prod_precio);

		if (holder.Comanda.isComentario()){
			holder.producto.setBackgroundColor(Color.YELLOW);
			LinearLayout Layoutabajo=(LinearLayout)row.findViewById(R.id.layabajo);
			Layoutabajo.setVisibility(LinearLayout.GONE);
		}

		if (holder.Comanda.isImpresa()){
			holder.producto.setBackgroundColor(Color.GREEN);
			holder.anadirButton.setVisibility(ImageButton.INVISIBLE);
			holder.quitarButton.setVisibility(ImageButton.INVISIBLE);
			holder.restarButton.setVisibility(ImageButton.INVISIBLE);
			holder.cantidad.setFocusable(false);			
		} else {
			holder.cantidad.setFocusable(true);	
			holder.cantidad.setOnEditorActionListener(new OE(position, holder.precio));
		}

		row.setTag(holder);
		setupItem(holder);
		return row;
	}


	class OE implements OnEditorActionListener  {
		private int position;
		TextView precio;

		public OE (int position, TextView precio) {
			this.position = position;
			this.precio = precio;
		}

		@Override
		public boolean onEditorAction(TextView tv, int arg1, KeyEvent arg2) {
			ComandaHolder holder = new ComandaHolder();
			holder.Comanda = items.get(position);

			int cant = 1;
			if (!tv.getText().toString().equals("")) {
				cant = Integer.parseInt(tv.getText().toString());
				if (cant < 1) {
					cant = 1;
					tv.setText("1");
				}
			} else {
				cant = 1;
				tv.setText("1");
			}

			holder.Comanda.setCantidad(cant);
			precio.setText(decim.format(holder.Comanda.getPreciounidad() * cant));
			return false;
		}
	}


	private void setupItem(ComandaHolder holder) {
		String var =  holder.Comanda.getVariedad();
		String nom = holder.Comanda.getNombre();
		String punto = holder.Comanda.getPunto();
		String tipo = holder.Comanda.getTipo();

		if (!var.equals("")){
			var = " (" + var + ")";
		}
		if (!punto.equals("")){
			punto = " (" + punto + ")";
		}
		holder.producto.setText(nom + var + punto);
		int cant = holder.Comanda.getCantidad();
		holder.cantidad.setText(String.valueOf(cant));
		holder.precio.setText(decim.format(holder.Comanda.getPreciounidad() * cant));

		if (tipo.equals("1")) {
			holder.tipo.setText("1�");
		} else if (tipo.equals("2")) {
			holder.tipo.setText("2�");
		} else if (tipo.equals("3")) {
			holder.tipo.setText("P");
		} else {
			holder.tipo.setText("");
		}
	}


	public static class ComandaHolder {
		Comanda Comanda;
		TextView producto;
		EditText cantidad;
		ImageButton anadirButton;
		ImageButton restarButton;
		ImageButton quitarButton;
		TextView tipo;
		TextView precio;
	}
}